#!/bin/bash
_HERE="$(dirname "$(readlink -f "$0")")"
if [ "${_HERE}" = "." ]; then
  _HERE="$(pwd)"
fi
cd "${_HERE}" || exit 1

USER_ID="$(id -u)"
GROUP_ID="$(id -g)"

if [ ! -f "${_HERE}/.env" ];then
    cp "${_HERE}/env.template" "${_HERE}/.env"
fi
sed -i "s/USER_ID=1000/USER_ID=${USER_ID}/g" "${_HERE}/.env"
sed -i "s/GROUP_ID=1000/GROUP_ID=${GROUP_ID}/g" "${_HERE}/.env"
sed -i "s/USER_ID=1000/USER_ID=${USER_ID}/g" "${_HERE}/docker-compose.yml"
sed -i "s/GROUP_ID=1000/GROUP_ID=${GROUP_ID}/g" "${_HERE}/docker-compose.yml"
if [ ! -f "${_HERE}/certs/client.crt" ]; then
  . "${_HERE}/.env"
  export COMMON_NAME
  echo "creating client and server certificates with common name: ${COMMON_NAME}"
  bash "${_HERE}/certs/make_certs.sh"
  echo "make sure you distribute the appropriate .crt and .key files correctly"
fi
