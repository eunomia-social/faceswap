from __future__ import absolute_import
import os
import shutil
import subprocess
import sys
import time
import uuid
from typing import Optional

import pytest
from httpx import AsyncClient
from asgi_lifespan import LifespanManager

os.environ["GPU_RUNNER_SOCKET"] = "localhost:5081"
os.environ["CERT_PREFIX"] = "test_"
os.environ["APP_ENV"] = "testing"
TESTING_TOKEN = uuid.uuid4().hex
os.environ["TESTING_TOKEN"] = TESTING_TOKEN

from grpc_client.main import app  # noqa

TEST_DATA_DIR = str(os.path.realpath(os.path.join(os.path.dirname(__file__), "data")))
worker_process: Optional[subprocess.Popen] = None
grpc_server_process: Optional[subprocess.Popen] = None


def _files_setup():
    os.makedirs(os.path.join(TEST_DATA_DIR, "files"), exist_ok=True)
    os.makedirs(os.path.join(TEST_DATA_DIR, "certs"), exist_ok=True)
    for entry in os.listdir(os.path.join(TEST_DATA_DIR, "files")):
        src = os.path.join(TEST_DATA_DIR, "files", entry)
        if os.path.isdir(src):
            shutil.rmtree(src)
        else:
            os.remove(src)
    shutil.rmtree(os.path.join(TEST_DATA_DIR, "certs"))
    for entry in os.listdir(os.path.join(TEST_DATA_DIR, "..", "initial_data")):
        src = os.path.realpath(os.path.join(TEST_DATA_DIR, "..", "initial_data", entry))
        dst = os.path.realpath(os.path.join(TEST_DATA_DIR, "files", entry))
        if entry == "certs":
            dst = os.path.realpath(os.path.join(TEST_DATA_DIR, entry))
        if os.path.isdir(src):
            shutil.copytree(src=src, dst=dst)
        else:
            shutil.copyfile(src=src, dst=dst)
    os.makedirs(os.path.join(TEST_DATA_DIR, "files", "results"), exist_ok=True)
    if os.path.exists("/tmp/redis.db"):  # pragma: nocover
        os.remove("/tmp/redis.db")


def _before():
    global worker_process
    global grpc_server_process
    worker_process = subprocess.Popen(
        [shutil.which("arq"), "worker.WorkerSettings"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        cwd=os.path.join(
            os.path.dirname(__file__),
            "..",
            "..",
            "..",
            "..",
            "docker",
            "gpu",
            "grpc_server",
        ),
        env={"APP_ENV": "testing"},
    )
    grpc_server_process = subprocess.Popen(
        [sys.executable, "server.py"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        cwd=os.path.join(
            os.path.dirname(__file__),
            "..",
            "..",
            "..",
            "..",
            "docker",
            "gpu",
            "grpc_server",
        ),
        env={"APP_ENV": "testing", "CERT_PREFIX": "test_"},
    )
    time.sleep(10)


def _after():
    global worker_process
    global grpc_server_process
    if worker_process:
        worker_process.kill()
    if grpc_server_process:
        grpc_server_process.kill()


@pytest.fixture
def data_test_path():
    return TEST_DATA_DIR


@pytest.fixture
def access_token():
    return TESTING_TOKEN


@pytest.fixture
async def client():
    async with AsyncClient(
        app=app, base_url="http://reface-client"
    ) as async_client, LifespanManager(app):
        yield async_client


@pytest.fixture
async def upload_image_request():
    async def _callable(filepath, client, request_headers):
        filename = os.path.basename(filepath)
        _, extension = os.path.splitext(filename)
        image_type = extension.split(".")[1]
        return await client.post(
            "/reface/upload",
            files={"file": (filename, open(filepath, "rb"), f"image/{image_type}")},
            headers=request_headers,
        )

    return _callable


@pytest.fixture
async def get_a_video():
    async def _callable(client, request_headers):
        response = await client.get("/reface/videos", headers=request_headers)
        assert response.status_code == 200
        response_data = response.json()
        assert len(response_data) > 0
        assert "name" in response_data[0] and "preview" in response_data[0]
        return response_data[0]

    return _callable


@pytest.fixture
def request_headers(access_token):
    return {"X-SN-URL": "-", "X-SN-TOKEN": access_token}


@pytest.fixture(autouse=True, scope="session")
def setup_fixture():
    _files_setup()
    _before()
    yield
    _after()
    _files_setup()
