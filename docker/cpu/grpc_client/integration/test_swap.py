import pytest


async def _valid_swap(get_a_video, client, request_headers):
    a_video = await get_a_video(client, request_headers)
    response = await client.get(
        f"/reface/swap?image={a_video['preview']}&video={a_video['name']}",
        headers=request_headers,
        timeout=30,
    )
    assert response.status_code == 200
    return response.json()


@pytest.mark.asyncio
async def test_swap_request_invalid_image(get_a_video, client, request_headers):
    a_video = await get_a_video(client, request_headers)
    response = await client.get(
        f"/reface/swap?image=invalid&video={a_video['name']}",
        headers=request_headers,
        timeout=30,
    )
    assert response.status_code != 200


@pytest.mark.asyncio
async def test_swap_request_invalid_video(get_a_video, client, request_headers):
    a_video = await get_a_video(client, request_headers)
    response = await client.get(
        f"/reface/swap?image={a_video['preview']}&video=invalid",
        headers=request_headers,
        timeout=30,
    )
    assert response.status_code != 200


@pytest.mark.asyncio
async def test_swap_request(get_a_video, client, request_headers):
    await _valid_swap(get_a_video, client, request_headers)


@pytest.mark.asyncio
async def test_swap_invalid_job_status(client, request_headers):
    response = await client.get(
        "/reface/status/invalid",
        headers=request_headers,
        timeout=30,
    )
    assert response.status_code == 400


@pytest.mark.asyncio
async def test_swap_job_status(get_a_video, client, request_headers):
    response = await _valid_swap(get_a_video, client, request_headers)
    job_id = response["job_id"]
    response = await client.get(
        f"/reface/status/{job_id}", headers=request_headers, timeout=30
    )
    assert response.status_code == 200
