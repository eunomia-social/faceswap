import pytest


@pytest.mark.asyncio
async def test_root_redirect(client):
    response = await client.get("/")
    assert response.status_code == 307


@pytest.mark.asyncio
async def test_docs_redirect(client):
    response = await client.get("/reface")
    assert response.status_code == 307
    assert response.headers["Location"] == "/reface/docs"


@pytest.mark.asyncio
async def test_get_videos(client, request_headers):
    response = await client.get("/reface/videos", headers=request_headers)
    assert response.status_code == 200
    response_data = response.json()
    assert len(response_data) > 0
    assert "name" in response_data[0]


@pytest.mark.asyncio
async def test_get_videos_without_token(client):
    response = await client.get("/reface/videos")
    assert response.status_code == 403


@pytest.mark.asyncio
async def test_get_videos_with_invalid_token(client):
    response = await client.get(
        "/reface/videos", headers={"X-SN-URL": "-", "X-SN-TOKEN": "invalid"}
    )
    assert response.status_code == 401
