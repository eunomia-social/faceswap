import os
import pytest


async def _delete_request(filename, client, request_headers):
    return await client.delete(
        f"/reface/files/{filename}",
        headers=request_headers,
    )


@pytest.mark.asyncio
async def test_upload_image(
    upload_image_request, get_a_video, data_test_path, client, request_headers
):
    filepath = os.path.join(data_test_path, "files", "man.png")
    response = await upload_image_request(filepath, client, request_headers)
    assert response.status_code == 200
    uploaded_file = response.json()
    await _delete_request(uploaded_file, client, request_headers)


@pytest.mark.asyncio
async def test_upload_no_image(
    upload_image_request, data_test_path, client, request_headers
):
    filepath = os.path.join(data_test_path, "files", "not_an_image.doc")
    response = await upload_image_request(filepath, client, request_headers)
    assert response.status_code == 400


@pytest.mark.asyncio
async def test_download_video_no_auth(get_a_video, client, request_headers):
    _video = await get_a_video(client, request_headers)
    filename = _video["name"]
    response = await client.get(f"/reface/files/{filename}")
    assert response.status_code == 422


@pytest.mark.asyncio
async def test_download_video_invalid_auth(get_a_video, client, request_headers):
    _video = await get_a_video(client, request_headers)
    filename = _video["name"]
    response = await client.get(f"/reface/files/{filename}?token=invalid&domain=any")
    assert response.status_code == 401


@pytest.mark.asyncio
async def test_download_video(get_a_video, client, request_headers):
    _video = await get_a_video(client, request_headers)
    filename = _video["name"]
    response = await client.get(
        f"/reface/files/{filename}?token={request_headers['X-SN-TOKEN']}&domain={request_headers['X-SN-URL']}"
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_download_image(get_a_video, client, request_headers):
    _video = await get_a_video(client, request_headers)
    filename = _video["preview"]
    response = await client.get(
        f"/reface/files/{filename}?token={request_headers['X-SN-TOKEN']}&domain={request_headers['X-SN-URL']}"
    )
    assert response.status_code == 200


@pytest.mark.asyncio
async def test_download_invalid_file(client, request_headers):
    response = await client.get(
        f"/reface/files/invalid?token={request_headers['X-SN-TOKEN']}&domain={request_headers['X-SN-URL']}"
    )
    assert response.status_code == 404


@pytest.mark.asyncio
async def test_delete_invalid_file(client, request_headers):
    response = await _delete_request("invalid", client, request_headers)
    assert response.status_code == 400
