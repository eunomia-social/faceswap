"""-*- coding: utf-8 -*-."""
import math
import os
import uuid
import mimetypes
from pathlib import Path

from mastodon import Mastodon as MastodonPy, MastodonError  # noqa
import orjson
import requests
from typing import Union, Tuple, Dict, Any
from fastapi import UploadFile, Depends, HTTPException, status
from fastapi.encoders import jsonable_encoder
from pydantic import AnyHttpUrl
from fastapi.security import APIKeyHeader
from google.protobuf.json_format import MessageToJson  # noqa

CERT_PREFIX = os.environ.get("CERT_PREFIX", "")
if not os.path.exists(
    os.path.join(os.path.dirname(__file__), f"{CERT_PREFIX}server.crt")
):
    cert_root = os.path.realpath(
        os.path.join(os.path.dirname(__file__), "..", "..", "..", "certs")
    )
    if os.environ.get("APP_ENV", "production") == "testing":
        cert_root = os.path.realpath(
            os.path.join(os.path.dirname(__file__), "integration", "data", "certs")
        )
    CERT_PREFIX = str(cert_root) + "/" + CERT_PREFIX
CHUNK_SIZE = 1024 * 1024
ESN_GATEWAY_ADDRESS = os.environ.get("ESN_GATEWAY_ADDRESS", "-")
ESN_GATEWAY_PORT = os.environ.get("ESN_GATEWAY_PORT", "-")
REQUEST_TIMEOUT = 30
api_key_sn_token_header = APIKeyHeader(
    name="X-SN-TOKEN", scheme_name="Social_Network_Token_Header", auto_error=True
)
api_key_sn_url_header = APIKeyHeader(
    name="X-SN-URL", scheme_name="Social_Network_Url_Header", auto_error=True
)

try:
    import reface_pb2 as models  # noqa
    import reface_pb2_grpc as rpc  # noqa
except ImportError:
    import sys

    sys.path.append(
        os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
    )  # noqa
    from grpc_client import reface_pb2 as models  # noqa
    from grpc_client import reface_pb2_grpc as rpc  # noqa


def load_secrets() -> Tuple[bytes, bytes, bytes]:  # noqa
    with open(f"{CERT_PREFIX}server.crt", "rb") as f:
        root_certificates = f.read()
    with open(f"{CERT_PREFIX}client.key", "rb") as f:
        private_key = f.read()
    with open(f"{CERT_PREFIX}client.crt", "rb") as f:
        cert_chain = f.read()
    return private_key, cert_chain, root_certificates


async def save_chunks_to_file(chunks, filename):
    with open(filename, "wb") as f:
        async for chunk in chunks:
            f.write(chunk.buffer)


def plain_domain(uri: Union[str, AnyHttpUrl]) -> str:
    """Get the domain name only from a url string."""
    text = uri if isinstance(uri, str) else str(uri)
    domain = text.split("//")[-1].split("/")[0]
    return domain


def validate_mastodon_user(mastodon_domain: str, mastodon_token: str) -> bool:
    """Get the mastodon user using the provided mastodon token."""
    if os.environ.get("APP_ENV", "production") == "testing":
        return mastodon_token == os.environ.get("TESTING_TOKEN", uuid.uuid4().hex)
    try:
        url = f"https://{mastodon_domain}/api/v1/accounts/verify_credentials"
        headers = {"Authorization": f"Bearer {mastodon_token}"}
        response = requests.get(url, headers=headers, timeout=REQUEST_TIMEOUT)
        return response.ok
    # pylint: disable=broad-except
    except (requests.Timeout, Exception):
        pass
    return False


def validate_user_token(
    x_api_key_token: str = Depends(api_key_sn_token_header),
    x_api_key_url: AnyHttpUrl = Depends(api_key_sn_url_header),
):
    """Check if the request user, is using a valid mastodon/sn token"""
    if validate_mastodon_user(
        mastodon_domain=plain_domain(x_api_key_url),
        mastodon_token=x_api_key_token,
    ):
        return x_api_key_token
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Not authenticated",
    )


def remove_file(path: str) -> None:
    """Remove a file (used as a BackgroundTask, after sending the file response)."""
    if os.path.exists(path):
        os.unlink(path)


def video_response(video: models.VideoResponse) -> Dict[str, Any]:
    video_dict = orjson.loads(MessageToJson(video))
    name = video_dict.pop("video", {}).get("name", None)
    if name:
        video_dict["name"] = name
    return video_dict


def _emit_lock(remove: bool = False) -> bool:
    _exists = os.path.exists(".lock")
    if remove:
        if _exists:
            os.unlink(".lock")
        return _exists
    else:
        if not _exists:
            with open(".lock", "w") as _lock:
                _lock.write("-")
        return not _exists


def emit_result(result: models.VideoResponse, sn_token: str):
    """Send the result back to client.

    Make an internal post request to esn-gateway (/event),
    so that it in turn emits the result to connected wss client
    """
    if _emit_lock():
        if ESN_GATEWAY_ADDRESS != "-" and ESN_GATEWAY_PORT != "-":
            result_dict = video_response(result)
            print("sending the result to the client:", result_dict)
            # only if we are in the same network with the esn_gateway
            # else, the clients can query the `/eunomia/reface/status/{job_id}` endpoint to get the result
            try:
                _port = int(ESN_GATEWAY_PORT)
            except (ValueError, Exception) as e:
                print(e)
                _emit_lock(True)
                return
            if math.isnan(_port):
                _emit_lock(True)
                return
            body = {"data": {"file": result_dict, "token": sn_token}, "type": "reface"}
            try:
                requests.post(
                    f"{ESN_GATEWAY_ADDRESS}:{ESN_GATEWAY_PORT}/event",
                    json=jsonable_encoder(body),
                    timeout=REQUEST_TIMEOUT,
                )
            except Exception as e:
                print(e)
        _emit_lock(True)


def upload_request_generator(
    uploaded_file: UploadFile,
):  # this generates the grpc `stream UploadRequest`
    file = uploaded_file.file
    uploaded_file.file.seek(0)
    while True:
        buffer = file.read(CHUNK_SIZE)
        if buffer:
            result = models.UploadRequest(
                data=models.Chunk(buffer=buffer),
                file=models.File(name=uploaded_file.filename),
            )
            yield result
        else:
            break


def share_file_to_mastodon(
    file_path: Path, mastodon_token: str, mastodon_domain: str
) -> str:
    try:
        instance = MastodonPy(
            access_token=mastodon_token,
            api_base_url=f"https://{mastodon_domain}",
            version_check_mode="none",
        )
        instance.account_verify_credentials()
        mime_type = mimetypes.guess_type(file_path)[0]
        image_dict = instance.media_post(media_file=str(file_path), mime_type=mime_type)
        image_id = image_dict.get("id", None)
        if not image_id:
            print("Could not upload media file")
            raise HTTPException(status_code=500, detail="Could not upload media file")
        response = instance.status_post(
            status="#faceswap",
            media_ids=[image_id],
            visibility="public",
        )
        if not response or not response.get("id", ""):
            raise (HTTPException(status_code=500, detail="Could post a new status"))
        return str(response.get("id", ""))

    except (MastodonError, Exception) as err:
        print(err)
        raise (HTTPException(status_code=500, detail="Could not share the file :("))
