"""-*- coding: utf-8 -*-."""
# pylint: disable=invalid-name,broad-except
import os
import tempfile
import json
import multiprocessing

workers_per_core_str = os.getenv("APP_MAX_WORKERS_PER_CORE", "1")
max_workers_str = os.getenv("APP_MAX_WORKERS", "1")
use_max_workers = None
if max_workers_str:
    use_max_workers = int(max_workers_str)

web_concurrency_str = os.getenv("APP_WEB_CONCURRENCY", "1")
max_requests_str = os.getenv("APP_MAX_REQUESTS", "0")
host = "0.0.0.0"
port = os.getenv("APP_PORT", "5080")
use_loglevel = os.getenv("LOG_LEVEL", "info")
cores = multiprocessing.cpu_count()
workers_per_core = float(workers_per_core_str)
default_web_concurrency = workers_per_core * cores
if web_concurrency_str:
    web_concurrency = int(web_concurrency_str)
    if web_concurrency <= 0:
        raise RuntimeError("not web_concurrency > 0")
else:
    web_concurrency = max(int(default_web_concurrency), 2)
    if use_max_workers:
        web_concurrency = min(web_concurrency, use_max_workers)
accesslog_var = os.getenv("ACCESS_LOG", "-")
use_accesslog = accesslog_var or None
errorlog_var = os.getenv("ERROR_LOG", "-")
use_errorlog = errorlog_var or None
graceful_timeout_str = os.getenv("GRACEFUL_TIMEOUT", "120")
timeout_str = os.getenv("TIMEOUT", "120")
keepalive_str = os.getenv("KEEP_ALIVE", "12")
# Gunicorn config variables
loglevel = use_loglevel
workers = web_concurrency
bind = f"{host}:{port}"
errorlog = use_errorlog
worker_tmp_dir = tempfile.gettempdir()
accesslog = use_accesslog
max_requests = 0
try:
    max_requests = min(int(max_requests_str), 1000)
    max_requests_jitter = 1000
except (ValueError, Exception):
    pass

graceful_timeout = int(graceful_timeout_str)
timeout = int(timeout_str)
keepalive = int(keepalive_str)
# For debugging and testing
log_data = {
    "loglevel": loglevel,
    "workers": workers,
    "bind": bind,
    "graceful_timeout": graceful_timeout,
    "timeout": timeout,
    "keepalive": keepalive,
    "errorlog": errorlog,
    "accesslog": accesslog,
    "host": host,
    "port": port,
}

print(json.dumps(log_data))
