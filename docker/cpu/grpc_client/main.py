"""-*- coding: utf-8 -*-."""
import os
import asyncio
import traceback
import tempfile
import orjson
from pathlib import Path
from typing import Any, List, Dict, Optional
from fastapi import FastAPI, File, UploadFile, Depends, HTTPException, status
from fastapi.responses import ORJSONResponse, FileResponse, RedirectResponse
from fastapi.middleware.cors import CORSMiddleware
from pydantic import AnyHttpUrl
from starlette.background import BackgroundTasks
from PIL import Image  # noqa
import grpc
from google.protobuf.json_format import MessageToJson  # noqa

try:  # pragma: nocover
    import reface_pb2 as models  # noqa
    import reface_pb2_grpc as rpc  # noqa
    from lib import (  # noqa
        load_secrets,
        validate_user_token,
        validate_mastodon_user,
        emit_result,
        remove_file,
        video_response,
        save_chunks_to_file,
        upload_request_generator,
        api_key_sn_url_header,
        plain_domain,
        share_file_to_mastodon,
    )
except ImportError:  # pragma: nocover
    import sys

    sys.path.append(
        os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "..."))
    )
    import grpc_client.reface_pb2 as models  # noqa
    import grpc_client.reface_pb2_grpc as rpc  # noqa
    from grpc_client.lib import (  # noqa
        load_secrets,
        validate_user_token,
        validate_mastodon_user,
        emit_result,
        remove_file,
        video_response,
        save_chunks_to_file,
        upload_request_generator,
        api_key_sn_url_header,
        plain_domain,
        share_file_to_mastodon,
    )

GPU_RUNNER_SOCKET = os.environ.get("GPU_RUNNER_SOCKET", "reface:5081")
# For more channel options, please see https://grpc.io/grpc/core/group__grpc__arg__keys.html
CHANNEL_OPTIONS = [
    ("grpc.lb_policy_name", "pick_first"),
    ("grpc.enable_retries", 0),
    ("grpc.keepalive_timeout_ms", 10000),
]


class RefaceApp(FastAPI):
    """Extend FastApi with a grpc.stub property."""

    stub: rpc.RefaceStub

    def __init__(self, *args, **kwargs):
        """Instance initialization."""
        super().__init__(*args, **kwargs)


app = RefaceApp(
    title="Reface gRPC client",
    description="Reface gRPC client",
    redoc_url=None,
    docs_url="/reface/docs",
    default_response_class=ORJSONResponse,
    openapi_url="/reface/openapi.json",
)

_allowed_origins = os.environ.get("ALLOWED_ORIGINS", "*")
origins = ["*"]
if _allowed_origins != "*":
    # coma separated hosts
    origins = _allowed_origins.split(",")
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


async def _get_job_status(job_id: str) -> Optional[models.JobStatusResponse]:
    try:
        response: models.JobStatusResponse = await app.stub.job_status(
            models.JobStatusRequest(JobId=job_id)
        )
        return response
    except grpc.RpcError as error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=error.details(),
        )


async def job_status_check(job_id: str, sn_token: str):
    """Connect to the remote worker's ws endpoint for the `job_id` and get its status."""
    result: Optional[models.VideoResponse] = None
    try:
        keep_running = True
        count = 0
        while keep_running:
            count += 1
            response: Optional[models.JobStatusResponse] = await _get_job_status(
                job_id=job_id
            )
            if (
                response is not None
                and response.result
                and orjson.loads(MessageToJson(response.result))
            ):
                result = response.result
                count += 120
            # 10 minutes
            keep_running = count < 120
            await asyncio.sleep(5)
    except Exception as e:
        print(e, type(e))
    if result:
        emit_result(result, sn_token)


async def _delete_file(filename: str):
    try:
        await app.stub.delete(models.DeleteRequest(Item=models.File(name=filename)))
    except grpc.RpcError as error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=error.details(),
        )
    except Exception as error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(error),
        )


async def _download_file(filename: str) -> Path:
    if not filename:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Bad request",
        )
    try:
        response = app.stub.download(
            models.DownloadRequest(file=models.File(name=filename))
        )
        if response is None:  # pragma: nocover
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="Bad request",
            )
        destination = Path(os.path.join(tempfile.gettempdir(), filename))
        await save_chunks_to_file(response, destination)
        return destination
    except grpc.RpcError as error:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail=error.details(),
        )
    except Exception as error:  # pragma: nocover
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(error),
        )


@app.head("/health", include_in_schema=False, status_code=204)
async def head_check():  # pragma: nocover
    """Health check."""
    return None


@app.get("/", include_in_schema=False)
async def root_path():
    """Not serving under root, redirect to docs."""
    return RedirectResponse(url="/reface/docs")


@app.get("/reface", include_in_schema=False)
async def reface_path():
    """Nothing to do here, redirect to docs."""
    return RedirectResponse(url="/reface/docs")


@app.get("/reface/videos")
async def get_available_videos(
    _: str = Depends(validate_user_token),
) -> List[Dict[str, str]]:
    """Get available video files."""
    try:
        response: models.VideosListResponse = await app.stub.get_videos(models.Empty())
        results = []
        for video in response.videos:
            results.append(video_response(video=video))
        return results
    except Exception as e:
        print(e, type(e))
    return []


@app.get("/reface/files/{filename}", response_class=FileResponse)
async def download_file(
    filename: str, background_tasks: BackgroundTasks, domain: str, token: str
):
    """Download a file if it exists on the remote worker."""
    if not validate_mastodon_user(mastodon_domain=domain, mastodon_token=token):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Not authenticated",
        )
    file_path = await _download_file(filename=filename)
    background_tasks.add_task(remove_file, file_path)
    return FileResponse(path=file_path, filename=filename)


@app.post("/reface/upload")
async def upload_file(
    file: UploadFile = File(...),
    _: str = Depends(validate_user_token),
) -> str:
    """Upload a new file."""
    try:
        with Image.open(file.file) as img:
            img.verify()
        response: models.File = await app.stub.upload(
            upload_request_generator(uploaded_file=file)
        )
        return response.name
    except grpc.RpcError as error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=error.details(),
        )
    except Exception as error:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(error),
        )


@app.delete("/reface/files/{filename}")
async def delete_file(
    filename: str,
    _: str = Depends(validate_user_token),
):
    """Delete a file."""
    await _delete_file(filename=filename)


@app.get("/reface/swap")
async def swap_faces(
    image: str,
    video: str,
    background_tasks: BackgroundTasks,
    token: str = Depends(validate_user_token),
) -> Any:
    """Request a combined video with the provided `image` and `video`.

    The `image` and `video` files must already exist on the remote runner.
    `video` should be one of the results returned using the `/reface/videos` endpoint
    `image` should be uploaded using the `/reface/upload` endpoint
    Remote runner:
        loads the image and the video files,
        does ml stuff
        returns the job's id that handles the face swap.
    """
    if not image or not video:  # pragma: nocover
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Bad request",
        )
        print("No image" if not image else "No video")
    try:
        response: models.SwapResponse = await app.stub.swap(
            models.SwapRequest(
                image=models.File(name=image), video=models.File(name=video)
            )
        )
        job_id = response.JobId
        if job_id:
            background_tasks.add_task(job_status_check, job_id, token)

        return {"job_id": job_id}
    except grpc.RpcError as error:
        print(error)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=error.details(),
        )
    except Exception as error:  # pragma: nocover
        print(error)
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=str(error),
        )


@app.post("/reface/share/{filename}")
async def share_file(
    filename: str,
    token: str = Depends(validate_user_token),
    x_api_key_url: AnyHttpUrl = Depends(api_key_sn_url_header),
):  # pragma: nocover
    """Share a result to Mastodon."""
    try:
        # save the file locally
        file_path = await _download_file(filename=filename)
        mastodon_domain = plain_domain(x_api_key_url)
        new_status_id = None
        if os.path.exists(file_path):
            new_status_id = share_file_to_mastodon(
                file_path=file_path,
                mastodon_token=token,
                mastodon_domain=mastodon_domain,
            )
        if not new_status_id:
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
                detail="Could not get the new status id",
            )
        # delete it from the server
        await _delete_file(filename=filename)
        # also from here (it is now uploaded to Mastodon)
        if os.path.exists(file_path):
            os.unlink(file_path)
        return {"id": new_status_id}
    except Exception as error:
        print("share_file():")
        traceback.format_exc()
        print(error)
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="Something went wrong :(",
        )


@app.get("/reface/status/{job_id}")
async def get_job_status(job_id: str, _: str = Depends(validate_user_token)):
    """Check a job's status."""
    response = await _get_job_status(job_id=job_id)
    if response:
        _response_dict = orjson.loads(MessageToJson(response))
        _response_result = _response_dict.pop("result", {})
        if _response_result:
            _name = _response_result.pop("video", {}).get("name", None)
            if _name:
                _response_result["name"] = _name
            _response_dict["result"] = _response_result
        return _response_dict
    raise HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST,
        detail="Bad request",
    )


@app.on_event("startup")
async def startup_event():
    """Initialize our grpc connection on startup."""
    try:
        private_key, certificate_chain, root_certificates = load_secrets()
        credentials = grpc.ssl_channel_credentials(
            root_certificates=root_certificates,
            private_key=private_key,
            certificate_chain=certificate_chain,
        )
        channel = grpc.aio.secure_channel(  # noqa
            target=GPU_RUNNER_SOCKET, options=CHANNEL_OPTIONS, credentials=credentials
        )
    except grpc.RpcError as error:
        sys.exit(f"Error: {error.details()}")
    else:
        app.stub = rpc.RefaceStub(channel=channel)
