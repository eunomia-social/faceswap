#!/bin/bash
_HERE="$(dirname "$(readlink -f "$0")")"
if [ "${_HERE}" = "." ]; then
  _HERE="$(pwd)"
fi

APP_ENV="${APP_ENV:-production}"
HOST="0.0.0.0"
PORT=${APP_PORT:-5080}
LOG_LEVEL=${LOG_LEVEL:-info}
WORKER_CLASS=${WORKER_CLASS:-"uvicorn.workers.UvicornWorker"}
GUNICORN_CONF="${_HERE}/gunicorn_conf.py"
if [ ! -f "${GUNICORN_CONF}" ]; then
    if [ -d "${_HERE}/grpc_client" ];then
        cd "${_HERE}/grpc_client" || exit 1
    fi
    GUNICORN_CONF="$(pwd)/gunicorn_conf.py"
fi
if [ ! -f "${GUNICORN_CONF}" ]; then
    exit 1
fi
if [ "${APP_ENV}" = "development" ]; then
    uvicorn --reload --host "$HOST" --port "$PORT" --log-level "$LOG_LEVEL" main:app
else
    gunicorn -k "$WORKER_CLASS" -c "$GUNICORN_CONF" main:app
fi
