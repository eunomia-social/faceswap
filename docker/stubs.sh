#!/usr/bin/env sh
HERE="$(dirname "$(readlink -f "$0")")"
if [ "${HERE}" = "." ];then
  HERE="$(pwd)"
fi
cd "${HERE}" || exit 1
python3 -m grpc_tools.protoc -I. --python_out=. --grpc_python_out=. --mypy_out=. reface.proto
cp reface_pb2* cpu/grpc_client/
cp reface_pb2* gpu/grpc_server/
rm reface_pb2*
