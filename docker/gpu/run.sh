#!/usr/bin/env bash
HERE="$(dirname "$(readlink -f "$0")")"
if [ "${HERE}" = "." ];then
  HERE="$(pwd)"
fi
cd "${HERE}/grpc_server" || exit 1

APP_ENV="${APP_ENV:-production}"
REFACE_PORT=${REFACE_PORT:-5081}

WHAT_TO_RUN="${1:-}"

if [ "${WHAT_TO_RUN}" = "--worker" ] ;then
  if [ "${APP_ENV}" = "development" ];then
    LD_PRELOAD=/usr/local/lib/libjemalloc.so arq worker.WorkerSettings --watch .
  else
    LD_PRELOAD=/usr/local/lib/libjemalloc.so arq worker.WorkerSettings
  fi
else
    LD_PRELOAD=/usr/local/lib/libjemalloc.so python server.py
fi
