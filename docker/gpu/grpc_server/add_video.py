"""-*- coding: utf-8 -*-."""
from __future__ import annotations, absolute_import
import argparse
import json

try:
    from .video import VideoFile, VideoFileInput, DimensionResize  # noqa
    from .settings import META_DIR  # noqa #
except ImportError:
    import sys
    import os

    sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__), "..")))
    from grpc_server.video import VideoFile, VideoFileInput, DimensionResize  # noqa
    from grpc_server.settings import META_DIR  # noqa

parser = argparse.ArgumentParser()
parser.add_argument(
    "filepath",
    type=VideoFileInput.file_path,
    help="The relative or absolute path of the file to be added",
)
parser.add_argument("-t", "--title", help="The video's title", default="Unknown")
parser.add_argument(
    "-nc",
    "--no-color",
    help="Is not a colorful video (greyscale)",
    action="store_false",
    dest="color",
    default=True,
)
parser.add_argument(
    "-ns",
    "--no-sound",
    help="The video does not include sound/audio",
    action="store_false",
    dest="sound",
    default=True,
)
parser.add_argument(
    "-pp",
    "--preview-position",
    help="The thumbnail position (in seconds) to grab",
    type=int,
    default=1,
)
parser.add_argument(
    "-j",
    "--json-config",
    help="The path of the json file to read the metadata",
    type=str,
    default=None,
)


def _main():
    args = parser.parse_args()
    _args_dict = args.__dict__
    _filepath = _args_dict["filepath"]
    _meta_file_arg = _args_dict.pop("json_config", None)
    meta_file = None
    if _meta_file_arg:
        if os.path.exists(_meta_file_arg):
            meta_file = os.path.realpath(_meta_file_arg)
        elif os.path.exists(
            os.path.join(os.path.dirname(_filepath), "meta", _meta_file_arg)
        ):
            meta_file = os.path.realpath(
                os.path.join(os.path.dirname(_filepath), "meta", _meta_file_arg)
            )
        elif os.path.exists(os.path.join(os.path.dirname(_filepath), _meta_file_arg)):
            meta_file = os.path.realpath(
                os.path.join(os.path.dirname(_filepath), _meta_file_arg)
            )
    if meta_file:
        video_file = VideoFile(_args_dict, meta_file=meta_file)
    else:
        video_file = VideoFile(_args_dict)
    video_file.save()
    print(json.dumps(video_file, indent=4))


if __name__ == "__main__":
    _main()
