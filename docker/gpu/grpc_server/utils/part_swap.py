"""-*- coding: utf-8 -*-."""
import yaml
import numpy as np
import torch
import torch.nn.functional as F
from tqdm import tqdm  # type: ignore
from .sync_batchnorm import DataParallelWithCallback  # type: ignore
from .modules.segmentation_module import SegmentationModule  # type: ignore
from .modules.reconstruction_module import ReconstructionModule  # type: ignore
from .modules.util import AntiAliasInterpolation2d  # type: ignore
from .modules.dense_motion import DenseMotionNetwork  # type: ignore
from .logger import load_reconstruction_module, load_segmentation_module  # type: ignore


class PartSwapGenerator(ReconstructionModule):
    def __init__(self, blend_scale=1.0, first_order_motion_model=False, **kwargs):
        super(PartSwapGenerator, self).__init__(**kwargs)
        if int(blend_scale) == 1:
            self.blend_downsample = lambda x: x
        else:
            self.blend_downsample = AntiAliasInterpolation2d(1, blend_scale)

        if first_order_motion_model:
            self.dense_motion_network = DenseMotionNetwork()
        else:
            self.dense_motion_network = None

    # noinspection PyMethodOverriding
    def forward(
        self,
        source_image,
        target_image,
        seg_target,
        seg_source,
        blend_mask,
        use_source_segmentation=False,
    ):  # type: ignore
        # Encoding of source image
        enc_source = self.first(source_image)
        for i in range(len(self.down_blocks)):
            enc_source = self.down_blocks[i](enc_source)

        # Encoding of target image
        enc_target = self.first(target_image)
        for i in range(len(self.down_blocks)):
            enc_target = self.down_blocks[i](enc_target)

        output_dict = {}
        motion = {}
        # Compute flow field for source image
        if self.dense_motion_network is None:
            segment_motions = self.segment_motion(seg_target, seg_source)
            segment_motions = segment_motions.permute(0, 1, 4, 2, 3)
            mask = seg_target["segmentation"].unsqueeze(2)
            deformation = (segment_motions * mask).sum(dim=1)
            deformation = deformation.permute(0, 2, 3, 1)
        else:
            motion = self.dense_motion_network(
                source_image=source_image, seg_target=seg_target, seg_source=seg_source
            )
            deformation = motion["deformation"]

        # Deform source encoding according to the motion
        enc_source = self.deform_input(enc_source, deformation)

        if self.estimate_visibility:
            if self.dense_motion_network is None:
                visibility = seg_source["segmentation"][:, 1:].sum(
                    dim=1, keepdim=True
                ) * (
                    1
                    - seg_target["segmentation"][:, 1:]
                    .sum(dim=1, keepdim=True)
                    .detach()
                )
                visibility = 1 - visibility
            else:
                visibility = motion["visibility"]

            if (
                enc_source.shape[2] != visibility.shape[2]
                or enc_source.shape[3] != visibility.shape[3]
            ):
                visibility = F.interpolate(
                    visibility, size=enc_source.shape[2:], mode="bilinear"
                )
            enc_source = enc_source * visibility

        blend_mask = self.blend_downsample(blend_mask)
        # If source segmentation is provided use it should be deformed before blending
        if use_source_segmentation:
            blend_mask = self.deform_input(blend_mask, deformation)

        out = enc_target * (1 - blend_mask) + enc_source * blend_mask

        out = self.bottleneck(out)

        for i in range(len(self.up_blocks)):
            out = self.up_blocks[i](out)

        out = self.final(out)
        out = F.sigmoid(out)

        output_dict["prediction"] = out

        return output_dict


def load_checkpoints(
    config, checkpoint, blend_scale=0.125, first_order_motion_model=False, cpu=False
):
    with open(config) as f:
        config = yaml.safe_load(f)

    reconstruction_module = PartSwapGenerator(  # type: ignore
        blend_scale=blend_scale,
        first_order_motion_model=first_order_motion_model,
        **config["model_params"]["reconstruction_module_params"],
        **config["model_params"]["common_params"],
    )

    if not cpu:
        reconstruction_module.cuda()

    segmentation_module = SegmentationModule(
        **config["model_params"]["segmentation_module_params"],
        **config["model_params"]["common_params"],
    )
    if not cpu:
        segmentation_module.cuda()

    if cpu:
        checkpoint = torch.load(checkpoint, map_location=torch.device("cpu"))
    else:
        checkpoint = torch.load(checkpoint)

    load_reconstruction_module(reconstruction_module, checkpoint)
    load_segmentation_module(segmentation_module, checkpoint)

    if not cpu:
        reconstruction_module = DataParallelWithCallback(reconstruction_module)
        segmentation_module = DataParallelWithCallback(segmentation_module)

    reconstruction_module.eval()
    segmentation_module.eval()

    return reconstruction_module, segmentation_module


def load_face_parser(cpu=False):
    from face_parsing.model import BiSeNet  # type: ignore

    face_parser = BiSeNet(n_classes=19)
    if not cpu:
        face_parser.cuda()
        face_parser.load_state_dict(torch.load("face_parsing/cp/79999_iter.pth"))
    else:
        face_parser.load_state_dict(
            torch.load(
                "face_parsing/cp/79999_iter.pth", map_location=torch.device("cpu")
            )
        )

    face_parser.eval()

    mean = torch.Tensor(np.array([0.485, 0.456, 0.406], dtype=np.float32)).view(
        1, 3, 1, 1
    )
    std = torch.Tensor(np.array([0.229, 0.224, 0.225], dtype=np.float32)).view(
        1, 3, 1, 1
    )

    if not cpu:
        face_parser.mean = mean.cuda()
        face_parser.std = std.cuda()
    else:
        face_parser.mean = mean
        face_parser.std = std

    return face_parser


def make_video(
    swap_index,
    source_image,
    target_video,
    reconstruction_module,
    segmentation_module,
    face_parser=None,
    hard=False,
    use_source_segmentation=False,
    cpu=False,
):
    if not isinstance(swap_index, list):
        raise ValueError(f"not a list: swap_index: {swap_index}")
    with torch.no_grad():
        predictions = []
        source = torch.tensor(source_image[np.newaxis].astype(np.float32)).permute(
            0, 3, 1, 2
        )
        if not cpu:
            source = source.cuda()
        target = torch.tensor(
            np.array(target_video)[np.newaxis].astype(np.float32)
        ).permute(0, 4, 1, 2, 3)
        seg_source = segmentation_module(source)

        for frame_idx in tqdm(range(target.shape[2])):
            target_frame = target[:, :, frame_idx]
            if not cpu:
                target_frame = target_frame.cuda()

            seg_target = segmentation_module(target_frame)

            # Computing blend mask
            if face_parser is not None:
                blend_mask = F.interpolate(
                    source if use_source_segmentation else target_frame, size=(512, 512)
                )
                blend_mask = (blend_mask - face_parser.mean) / face_parser.std
                blend_mask = torch.softmax(face_parser(blend_mask)[0], dim=1)
            else:
                blend_mask = (
                    seg_source["segmentation"]
                    if use_source_segmentation
                    else seg_target["segmentation"]
                )

            blend_mask = blend_mask[:, swap_index].sum(dim=1, keepdim=True)
            if hard:
                blend_mask = (blend_mask > 0.5).type(blend_mask.type())

            out = reconstruction_module(
                source,
                target_frame,
                seg_source=seg_source,
                seg_target=seg_target,
                blend_mask=blend_mask,
                use_source_segmentation=use_source_segmentation,
            )

            predictions.append(
                np.transpose(out["prediction"].data.cpu().numpy(), [0, 2, 3, 1])[0]
            )
        return predictions
