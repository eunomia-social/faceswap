from __future__ import absolute_import
import os
import sys

import pytest

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))  # noqa
from grpc_server.video import VideoFileInput  # noqa


def test_file_input_without_path():
    with pytest.raises(ValueError):
        _ = VideoFileInput({})


def test_file_input_with_invalid_path():
    with pytest.raises(FileNotFoundError):
        _ = VideoFileInput({"filepath": "invalid"})


def test_file_input_with_valid_path(input_args):
    video_file, video_meta_file, video_meta_json = input_args
    video_file_input = VideoFileInput({"filepath": video_file})
    assert video_file_input


def test_file_input_not_a_video(data_test_path):
    _uploads_dir = os.path.join(data_test_path, "files")
    _invalid_file_path = os.path.join(_uploads_dir, "not_a_video.mp4")
    with pytest.raises(ValueError):
        _ = VideoFileInput({"filepath": _invalid_file_path})


def test_file_input_image_instead_of_video(data_test_path):
    _uploads_dir = os.path.join(data_test_path, "files")
    _invalid_file_path = os.path.join(_uploads_dir, "faces", "man.png")
    with pytest.raises(ValueError):
        _ = VideoFileInput({"filepath": _invalid_file_path})
