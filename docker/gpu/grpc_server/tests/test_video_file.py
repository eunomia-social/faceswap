from __future__ import absolute_import
import os
import sys

import pytest


sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))  # noqa
from grpc_server.video import VideoFile  # noqa


def test_video_file_invalid_init():
    with pytest.raises(ValueError):
        VideoFile()


def test_video_file_invalid_meta():
    with pytest.raises(ValueError):
        VideoFile(meta_file="Invalid")


def test_video_file_from_meta(input_args):
    video_file_path, video_meta_file, video_meta_json = input_args
    video_file = VideoFile(meta_file=video_meta_file)
    assert video_file.filename == os.path.basename(video_file_path)


def test_valid_video_file(input_args):
    video_file_path, video_meta_file, video_meta_json = input_args
    video_file = VideoFile(dict_args={"filepath": video_file_path})
    assert video_file.filename == os.path.basename(video_file_path)


def test_video_file_get_meta(input_args):
    video_file_path, video_meta_file, video_meta_json = input_args
    video_file = VideoFile(meta_file=video_meta_file, force_meta=True)
    assert video_file.filename == os.path.basename(video_file_path)


def test_video_file_without_extension(data_test_path):
    video_file_path = os.path.join(data_test_path, "files", "video_file")
    video_file = VideoFile(dict_args={"filepath": video_file_path})
    assert video_file.filename == "video_file"


def test_video_file_without_audio(data_test_path):
    video_file_path = os.path.join(data_test_path, "files", "muted_video.mp4")
    _ = VideoFile(dict_args={"filepath": video_file_path})
