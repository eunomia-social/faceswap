from __future__ import absolute_import
import os
import shutil
import tempfile
import uuid
from pathlib import Path
import grpc

import pytest
from PIL import Image  # noqa
from pytest_grpc import aio_available  # noqa

try:
    import reface_pb2 as models  # noqa
except ImportError:
    import sys

    sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__), "..")))
    import reface_pb2 as models  # noqa


async def _upload_file(name, data_test_path, aio_grpc_stub, upload_request_generator):
    response: models.File = await aio_grpc_stub.upload(upload_request_generator(name))
    assert (
        response.name.endswith(".png")
        and len(response.name) == len(uuid.uuid4().hex) + 4
    )
    uploaded_file_path = os.path.join(data_test_path, "files", response.name)
    assert os.path.exists(uploaded_file_path)
    return uploaded_file_path


async def _swap_job(
    aio_grpc_stub, input_args, data_test_path, upload_request_generator
):
    uploaded_file = await _upload_file(
        name="man.png",
        data_test_path=data_test_path,
        aio_grpc_stub=aio_grpc_stub,
        upload_request_generator=upload_request_generator,
    )
    video_file_path, _, __ = input_args
    image_file_name = os.path.basename(uploaded_file)
    video_file_name = os.path.basename(video_file_path)
    request = models.SwapRequest(
        image=models.File(name=image_file_name), video=models.File(name=video_file_name)
    )
    response: models.SwapResponse = await aio_grpc_stub.swap(request)
    assert response.JobId
    return response


@aio_available
@pytest.mark.asyncio
async def test_get_videos(aio_grpc_stub):
    request = models.Empty()
    response = await aio_grpc_stub.get_videos(request)
    assert response


@aio_available
@pytest.mark.asyncio
async def test_upload_file(aio_grpc_stub, data_test_path, upload_request_generator):
    await _upload_file(
        name="woman.png",
        data_test_path=data_test_path,
        aio_grpc_stub=aio_grpc_stub,
        upload_request_generator=upload_request_generator,
    )


@aio_available
@pytest.mark.asyncio
async def test_download_file(aio_grpc_stub, data_test_path, upload_request_generator):
    uploaded_file = await _upload_file(
        name="man.png",
        data_test_path=data_test_path,
        aio_grpc_stub=aio_grpc_stub,
        upload_request_generator=upload_request_generator,
    )
    file_name = os.path.basename(uploaded_file)
    chunks = aio_grpc_stub.download(
        models.DownloadRequest(file=models.File(name=file_name))
    )
    new_name = uuid.uuid4().hex
    destination = Path(os.path.join(tempfile.gettempdir(), new_name))
    with open(destination, "wb") as f:
        async for chunk in chunks:
            f.write(chunk.buffer)
    with Image.open(destination) as img:
        img.verify()
    os.remove(destination)


@aio_available
@pytest.mark.asyncio
async def test_download_invalid_file(aio_grpc_stub):
    with pytest.raises(grpc.aio.AioRpcError):
        chunks = aio_grpc_stub.download(
            models.DownloadRequest(file=models.File(name="invalid"))
        )
        async for chunk in chunks:
            assert not chunk


@aio_available
@pytest.mark.asyncio
async def test_add_swap_job(
    aio_grpc_stub, input_args, data_test_path, upload_request_generator
):
    await _swap_job(aio_grpc_stub, input_args, data_test_path, upload_request_generator)


@aio_available
@pytest.mark.asyncio
async def test_get_job_status(
    aio_grpc_stub, input_args, data_test_path, upload_request_generator
):
    job = await _swap_job(
        aio_grpc_stub, input_args, data_test_path, upload_request_generator
    )
    request = models.JobStatusRequest(JobId=job.JobId)
    job_status: models.JobStatusResponse = await aio_grpc_stub.job_status(request)
    assert job_status.status == "queued"


@aio_available
@pytest.mark.asyncio
async def test_get_invalid_job_status(aio_grpc_stub):
    request = models.JobStatusRequest(JobId="invalid")
    with pytest.raises(grpc.aio.AioRpcError):
        await aio_grpc_stub.job_status(request)


@aio_available
@pytest.mark.asyncio
async def test_delete_file_with_meta(aio_grpc_stub, add_video, input_args):
    # create a new copy before deleting
    add_video()
    _, __, video_meta_json = input_args
    request = models.DeleteRequest(Item=models.File(name=video_meta_json))
    await aio_grpc_stub.delete(request)
    assert not os.path.exists(video_meta_json)


@aio_available
@pytest.mark.asyncio
async def test_delete_invalid_file(aio_grpc_stub):
    with pytest.raises(grpc.aio.AioRpcError):
        request = models.DeleteRequest(Item=models.File(name="invalid"))
        await aio_grpc_stub.delete(request)


@aio_available
@pytest.mark.asyncio
async def test_delete_file_by_name(aio_grpc_stub, add_video, input_args):
    # create a new copy before deleting
    add_video()
    video_file, video_meta_file, video_meta_json = input_args
    request = models.DeleteRequest(Item=models.File(name=os.path.basename(video_file)))
    await aio_grpc_stub.delete(request)
    assert not os.path.exists(video_meta_json)


@aio_available
@pytest.mark.asyncio
async def test_delete_results(aio_grpc_stub, data_test_path, add_video):
    # create a new copy before deleting
    add_video()
    if not os.listdir(os.path.join(data_test_path, "files", "results")):
        _src_name = os.listdir(os.path.join(data_test_path, "files", "meta"))[0]
        src = os.path.join(data_test_path, "files", "meta", _src_name)
        dst = os.path.join(data_test_path, "files", "results", _src_name)
        shutil.move(src=src, dst=dst)
    assert os.listdir(os.path.join(data_test_path, "files", "results"))
    request = models.DeleteRequest()
    await aio_grpc_stub.delete(request)
    assert not os.listdir(os.path.join(data_test_path, "files", "results"))
