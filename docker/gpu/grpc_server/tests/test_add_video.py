from __future__ import absolute_import
import sys
import subprocess


def test_add_invalid_video_file(add_video_script_path):
    process = subprocess.Popen(
        [sys.executable, add_video_script_path, "invalid_file"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    process.communicate()
    assert process.returncode == 1


def test_add_video_file(add_video_script_path, input_args):
    video_file, video_meta_file, video_meta_json = input_args
    process = subprocess.Popen(
        [
            sys.executable,
            add_video_script_path,
            "-j",
            video_meta_file,
            video_file,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    process.communicate()
    assert process.returncode == 0


def test_add_video_file_find_meta(add_video_script_path, input_args):
    video_file, video_meta_file, video_meta_json = input_args
    process = subprocess.Popen(
        [
            sys.executable,
            add_video_script_path,
            "-j",
            video_meta_json,
            video_file,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    process.communicate()
    assert process.returncode == 0


def test_add_video_without_meta(add_video_script_path, input_args):
    video_file, video_meta_file, video_meta_json = input_args
    process = subprocess.Popen(
        [
            sys.executable,
            add_video_script_path,
            video_file,
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    process.communicate()
    assert process.returncode == 0
