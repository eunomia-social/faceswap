from __future__ import absolute_import

import shutil
import subprocess
import sys
import os

import pytest
import grpc

os.environ["CERT_PREFIX"] = "test_"
os.environ["APP_ENV"] = "testing"
sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from grpc_server.settings import get_redis_pool  # noqa
from grpc_server.video import VideoFile  # noqa
import grpc_server.reface_pb2 as models  # noqa

CHUNK_SIZE = 1024 * 1024
ADD_VIDEO_SCRIPT = os.path.realpath(
    os.path.join(os.path.dirname(__file__), "..", "add_video.py")
)
TEST_DATA_DIR = str(os.path.realpath(os.path.join(os.path.dirname(__file__), "data")))


def _files_setup():
    os.makedirs(os.path.join(TEST_DATA_DIR, "files"), exist_ok=True)
    os.makedirs(os.path.join(TEST_DATA_DIR, "certs"), exist_ok=True)
    for entry in os.listdir(os.path.join(TEST_DATA_DIR, "files")):
        src = os.path.join(TEST_DATA_DIR, "files", entry)
        if os.path.isdir(src):
            shutil.rmtree(src)
        else:
            os.remove(src)
    shutil.rmtree(os.path.join(TEST_DATA_DIR, "certs"))
    for entry in os.listdir(os.path.join(TEST_DATA_DIR, "..", "initial_data")):
        src = os.path.realpath(os.path.join(TEST_DATA_DIR, "..", "initial_data", entry))
        dst = os.path.realpath(os.path.join(TEST_DATA_DIR, "files", entry))
        if entry == "certs":
            dst = os.path.realpath(os.path.join(TEST_DATA_DIR, entry))
        if os.path.isdir(src):
            shutil.copytree(src=src, dst=dst)
        else:
            shutil.copyfile(src=src, dst=dst)
    os.makedirs(os.path.join(TEST_DATA_DIR, "files", "results"), exist_ok=True)
    if os.path.exists("/tmp/redis.db"):  # pragma: nocover
        os.remove("/tmp/redis.db")


def _import_rpc():
    try:
        import reface_pb2_grpc as rpc
        from server import Servicer
    except ImportError:  # pragma: nocover
        import sys

        sys.path.append(
            os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "..", "app"))
        )  # noqa
        import reface_pb2_grpc as rpc  # noqa
        from server import Servicer  # noqa
    return rpc, Servicer


@pytest.fixture
def data_test_path():
    return TEST_DATA_DIR


@pytest.fixture
def add_video_script_path():
    return ADD_VIDEO_SCRIPT


@pytest.fixture
def upload_request_generator():
    def _callable(name):
        _file = open(os.path.join(TEST_DATA_DIR, "files", "faces", name), "rb")
        _file.seek(0)
        while True:
            buffer = _file.read(CHUNK_SIZE)
            if buffer:
                result = models.UploadRequest(
                    data=models.Chunk(buffer=buffer),
                    file=models.File(name=name),
                )
                yield result
            else:  # pragma: nocover
                break

    return _callable


@pytest.fixture
def input_args():
    _uploads_dir = os.path.join(TEST_DATA_DIR, "files")
    video_meta_dir = os.path.join(_uploads_dir, "meta")
    if not os.listdir(video_meta_dir):
        _files_setup()
    video_meta_json = os.listdir(video_meta_dir)[0]
    video_meta_file = os.path.join(video_meta_dir, video_meta_json)
    video_file = os.path.join(_uploads_dir, video_meta_json.replace("json", "mp4"))
    return video_file, video_meta_file, video_meta_json


@pytest.fixture
def add_video(input_args):
    def _callable():
        video_file, video_meta_file, video_meta_json = input_args
        process = subprocess.Popen(
            [
                sys.executable,
                ADD_VIDEO_SCRIPT,
                "-j",
                video_meta_file,
                video_file,
            ],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        process.communicate()
        assert process.returncode == 0

    return _callable


@pytest.fixture
def key():
    """SSL private key"""
    with open(os.path.join(TEST_DATA_DIR, "certs", "test_server.key"), "rb") as f:
        return f.read()


@pytest.fixture
def cert():
    """Self-signed SSL root cert"""
    with open(os.path.join(TEST_DATA_DIR, "certs", "test_server.crt"), "rb") as f:
        return f.read()


@pytest.fixture
def grpc_add_to_server():
    rpc, _ = _import_rpc()

    return rpc.add_RefaceServicer_to_server


@pytest.fixture
def grpc_servicer():
    _, servicer = _import_rpc()
    return servicer


@pytest.fixture
def grpc_stub_cls():
    rpc, _ = _import_rpc()
    yield rpc.RefaceStub


@pytest.fixture
def grpc_server_credentials(key, cert):
    return grpc.ssl_server_credentials(
        [
            (key, cert),
        ]
    )


@pytest.fixture
def grpc_channel_credentials(cert):
    return grpc.ssl_channel_credentials(cert)


@pytest.yield_fixture
async def arq_redis():
    redis_ = await get_redis_pool()
    yield redis_
    redis_.close()
    await redis_.wait_closed()


@pytest.fixture(autouse=True, scope="session")
def my_fixture():
    _files_setup()
    yield
    _files_setup()
