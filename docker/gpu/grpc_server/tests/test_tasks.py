from __future__ import absolute_import
import os
import sys
import shutil

import pytest

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))  # noqa
from grpc_server.tasks import reface_task, cleanup_task  # noqa
from grpc_server.settings import UPLOADS_DIR, RESULTS, META  # noqa


def _before_cleanup(data_test_path):
    files_to_backup = ["muted_video.mp4", "not_a_video.mp4", "video_file"]
    for _file in files_to_backup:
        src = os.path.join(data_test_path, "files", _file)
        dst = os.path.join(f"/tmp/{_file}")
        shutil.copyfile(src=src, dst=dst)


def _after_cleanup(data_test_path):
    files_to_restore = ["muted_video.mp4", "not_a_video.mp4", "video_file"]
    for _file in files_to_restore:
        src = os.path.join(f"/tmp/{_file}")
        dst = os.path.join(data_test_path, "files", _file)
        shutil.copyfile(src=src, dst=dst)


@pytest.mark.asyncio
async def test_run_reface_task(data_test_path, input_args):
    video_file_path, video_meta_file, _ = input_args
    src = os.path.join(data_test_path, "files", "faces", "man.png")
    dst = os.path.join(data_test_path, "files", "man.png")
    shutil.copyfile(src=src, dst=dst)
    result = await reface_task(
        {},
        image_file="man.png",
        video_file=os.path.basename(video_file_path),
        meta_file=video_meta_file,
    )
    assert result


@pytest.mark.asyncio
async def test_run_cleanup_task(data_test_path):
    results_dir = os.path.join(UPLOADS_DIR, RESULTS)
    assert len(os.listdir(results_dir)) > 0
    _before_cleanup(data_test_path)
    src = os.path.join(data_test_path, "files", "faces", "man.png")
    dst = os.path.join(data_test_path, "files", "man.png")
    shutil.copyfile(src=src, dst=dst)
    await cleanup_task({})
    assert not os.path.exists(os.path.join(data_test_path, "files", "man.png"))
    assert not os.listdir(results_dir)
    assert len(os.listdir(os.path.join(UPLOADS_DIR, META))) > 0
    _after_cleanup(data_test_path)
