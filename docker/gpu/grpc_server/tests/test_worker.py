import asyncio
import os
import shutil
import subprocess
from datetime import timedelta
import signal

import pytest
from arq import ArqRedis
from pytest_grpc import aio_available  # noqa


@aio_available
@pytest.mark.asyncio
async def test_add_task(arq_redis):
    await arq_redis.enqueue_job(
        "dummy",
        _job_id="3",
        _expires=timedelta(seconds=3),
    )


def _start_worker():
    return subprocess.Popen(
        [shutil.which("arq"), "worker.WorkerSettings"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        cwd=os.path.join(os.path.dirname(__file__), ".."),
        env={"APP_ENV": "testing"},
    )


def test_start_worker_using_arq():
    proc = _start_worker()
    try:
        out, err = proc.communicate(timeout=20)
    except subprocess.TimeoutExpired:
        proc.kill()
        out, err = proc.communicate()
    assert "Starting worker for 2 functions: reface_task, cleanup_task" in err.decode()
    assert "redis_version" in err.decode()
    assert proc.returncode == -signal.SIGKILL.SIGKILL


@aio_available
@pytest.mark.asyncio
async def test_add_reface_task(arq_redis: ArqRedis, data_test_path, input_args):
    proc = _start_worker()
    video_file_path, video_meta_file, video_meta_json = input_args
    current_jobs = await arq_redis.queued_jobs()
    job = await arq_redis.enqueue_job(
        "reface_task",
        _expires=timedelta(minutes=15),
        image_file="faces/man.png",
        video_file=os.path.basename(video_file_path),
        meta_file=video_meta_file,
    )
    assert not current_jobs
    assert job
    await asyncio.sleep(20)
    try:
        result = await job.result(timeout=600)
        if not result:  # pragma: nocover
            # no gpu?
            status = await job.status()
            await job.abort()
        proc.kill()
    except subprocess.TimeoutExpired as e:  # pragma: nocover
        proc.kill()
        raise e
