"""-*- coding: utf-8 -*-."""
from __future__ import annotations, absolute_import
import shutil
import uuid
import math
from subprocess import check_output, STDOUT
import os
import orjson
from datetime import timedelta
from typing import Optional, Any, Dict, Union, final
import shlex
import numpy as np
import blurhash as bh  # noqa
import ffmpeg  # noqa
from ffmpeg import Error
from skimage.transform import resize  # noqa
from skimage import img_as_ubyte  # noqa
from PIL import Image  # noqa
from moviepy.video.VideoClip import TextClip  # noqa
from moviepy.video.compositing.CompositeVideoClip import CompositeVideoClip  # noqa
from moviepy.video.io.VideoFileClip import VideoFileClip  # noqa

from .video_file_input import VideoFileInput  # noqa

try:
    from grpc_server.settings import (  # noqa
        UPLOADS_DIR,  # noqa
        META,  # noqa
        RESULTS,  # noqa
        AUDIO_MAPPINGS,  # noqa
        MOVIE_PY_CODEC_MAPPINGS,  # noqa
        CAPTIONS_POST_FIX,  # noqa
    )
except ImportError:  # pragma: nocover
    import sys

    sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
    from grpc_server.settings import (  # noqa
        UPLOADS_DIR,  # noqa
        META,  # noqa
        RESULTS,  # noqa
        AUDIO_MAPPINGS,  # noqa
        MOVIE_PY_CODEC_MAPPINGS,  # noqa
        CAPTIONS_POST_FIX,  # noqa
    )


@final
class VideoFile(VideoFileInput):
    length: str
    duration: float
    width: int
    height: int
    bitrate: int
    size: str
    preview_position: int = 1
    frame_rate: Optional[str] = None
    audio_codec: Optional[str] = None
    audio_bitrate: Optional[str] = None
    blurhash: Optional[str] = None

    def __init__(
        self,
        dict_args: Optional[Dict[str, Any]] = None,
        meta_file: Optional[str] = None,
        force_meta: Optional[bool] = False,
    ):
        if not dict_args and not meta_file:
            raise ValueError("Invalid init args")
        meta_args: Optional[Dict[str, Any]] = None
        if meta_file is not None:
            meta_args = self.dict_from_meta(meta_file)
        if not dict_args and not meta_args:
            raise ValueError("Invalid init args")
        if dict_args is None:
            dict_args = {}
        if meta_args:
            for arg, val in meta_args.items():
                dict_args[arg] = val
        if not dict_args:  # pragma: nocover
            raise ValueError("Invalid init args")
        if "filepath" not in dict_args and "filename" in dict_args:
            filename: str = dict_args.pop("filename")
            dict_args["filepath"] = os.path.join(UPLOADS_DIR, filename)
        if "caption" not in dict_args:
            dict_args["caption"] = self.caption
        super(VideoFile, self).__init__(dict_args)
        self._get_properties(force=force_meta, save=False)
        self._dict()

    def _dict(self):
        # json dump-able (inherits `dict` from `VideoFileInput`)
        for entry in self.__dict__:
            self[entry] = self.__getattribute__(entry)
        self.pop("filepath")
        self.pop("preview_position")
        self["id"] = self.id
        self["filename"] = self.filename
        self["audio"] = self.audio
        self["muted"] = self.muted if os.path.exists(self.muted_path) else self.filename
        self["aspect_ratio"] = self.aspect_ratio
        self["preview"] = self.preview
        if hasattr(self, "bitrate") and self.bitrate:
            self["bitrate"] = self.bitrate

    @staticmethod
    def dict_from_meta(meta_file: Union[str, os.PathLike]) -> Optional[Dict[str, Any]]:
        _meta_path = os.path.join(UPLOADS_DIR, META, meta_file)
        if not os.path.exists(meta_file) and not os.path.exists(_meta_path):
            return None
        meta_path = (
            os.path.realpath(meta_file) if os.path.exists(meta_file) else _meta_path
        )
        return VideoFileInput.load_json(meta_path)

    @property
    def preview_path(self) -> str:
        _path, _ = os.path.splitext(self.filepath)
        return _path + ".jpg"

    @property
    def aspect_ratio(self) -> float:
        return self.width / self.height

    @property
    def preview(self) -> str:
        return os.path.basename(self.preview_path)

    @property
    def audio_path(self) -> str:
        file_base, video_extension = os.path.splitext(self.filepath)
        audio_extension = "m4a"
        if self.audio_codec and self.audio_codec in AUDIO_MAPPINGS:
            audio_extension = AUDIO_MAPPINGS.get(self.audio_codec, audio_extension)
        return file_base + f".{audio_extension}"

    @property
    def audio(self) -> str:
        return os.path.basename(self.audio_path)

    @property
    def muted_path(self):
        file_base, video_extension = os.path.splitext(self.filepath)
        if not video_extension:
            video_extension = ".mp4"
        return file_base + "_no_audio" + video_extension

    @property
    def muted(self):
        return os.path.basename(self.muted_path)

    @property
    def filename(self) -> str:
        return os.path.basename(self.filepath)

    def _make_preview(self, force: Optional[bool] = False):
        if not os.path.exists(self.preview_path) or force:
            ss = self.preview_position
            tp = self.preview_path
            command_str = f"ffmpeg -i {self.filepath} -y -vframes 1 -an -s {self.size} -ss {ss} {tp}"
            command = shlex.split(command_str)
            _ = (
                check_output(command, stderr=STDOUT, timeout=10)
                .decode()
                .replace("\n", "")
            )

    def _handle_audio(self, probe: Dict[str, Any]):
        audio = next(
            (stream for stream in probe["streams"] if stream["codec_type"] == "audio"),
            None,
        )
        if audio is not None:
            self.audio_bitrate = audio.get("sample_rate", None)
            if self.audio_bitrate:
                self.audio_bitrate += " Hz"
            self.audio_codec = audio.get("codec_name", None)
            self.split_audio_video()

    def _handle_video(self, probe: Dict[str, Any]):
        video = next(
            (stream for stream in probe["streams"] if stream["codec_type"] == "video"),
            None,
        )
        if not video:  # pragma: nocover
            raise TypeError("Invalid file")
        self.duration = float(video.get("duration", None))
        length = str(timedelta(seconds=self.duration))
        length_parts = length.split(":")
        length_str = ":".join(length_parts[:-1])
        sec_parts = length_parts[-1].split(".")
        if len(sec_parts) != 2:  # pragma: nocover
            length_str += ":" + length_parts[-1]
        else:
            length_str += ":" + sec_parts[0] + "." + sec_parts[1][:2]
        self.length = length_str
        self.frame_rate = video.get("avg_frame_rate", None)
        if self.frame_rate and "/" in self.frame_rate:
            self.frame_rate = self.frame_rate.split("/")[0]
        self.width = int(video.get("width", None))
        self.height = int(video.get("height", None))
        self.size = f"{self.width}x{self.height}"
        self.bitrate = int(video.get("bit_rate", "290048"))

    def get_blurhash(self):
        if not self.preview:  # pragma: nocover
            raise ValueError("No preview/image file 1")
        preview_path = os.path.join(os.path.dirname(self.filepath), self.preview)
        if not os.path.exists(preview_path):
            raise ValueError("No preview/image file 2")
        self.blurhash = bh.encode(np.array(Image.open(preview_path).convert("RGB")))

    def _check_image(self, force: Optional[bool] = None):
        if (
            force
            or not self.get("preview", None)
            or not os.path.exists(
                os.path.join(os.path.dirname(self.filepath), self.get("preview", None))
            )
        ):
            if not self.get("preview_position", None) or (
                self.get("preview_position", None) is not None
                and self.get("preview_position", None) > self.get("duration", math.inf)
            ):
                self.preview_position = int(min(self.get("duration", math.inf), 1))
            self._make_preview()

    def _get_properties(
        self, force: Optional[bool] = False, save: Optional[bool] = False
    ):
        check_video = bool(force)
        if not check_video:
            video_props = {"duration", "size", "length", "frame_rate", "bitrate"}
            check_video = any(self.get(prop, None) is None for prop in video_props)
        check_audio = bool(force)
        if not check_audio:
            audio_props = {"audio_bitrate", "audio_codec"}
            check_audio = any(self.get(prop, None) is None for prop in audio_props)
            if not check_audio and self.audio_codec is not None:
                check_audio = not os.path.exists(self.audio_path)
        probe: Optional[Dict[str, Any]] = None
        if check_video or check_audio:
            video_path = self.file_path(self.filepath)
            probe = ffmpeg.probe(video_path)
            if check_video and probe:
                self._handle_video(probe=probe)
        if check_audio and probe:
            self._handle_audio(probe=probe)
        self._check_image(force=force)
        if not self.blurhash or force:
            self.get_blurhash()
        if force and save:  # pragma: nocover
            self.save()

    def split_audio_video(self, force: Optional[bool] = False):
        """Split video and audio parts of the current file."""
        if (
            not os.path.exists(self.audio_path)
            or not os.path.exists(self.muted_path)
            or force
        ):
            stream = ffmpeg.input(self.filepath)
            if not stream.video or not stream.audio:  # pragma: nocover
                raise ValueError("Invalid video input")
            video_out = ffmpeg.output(stream.video, self.muted_path, c="copy")
            audio_out = ffmpeg.output(stream.audio, self.audio_path, c="copy")
            quiet = os.environ.get("APP_ENV", "production") == "production"
            ffmpeg.run(video_out, overwrite_output=True, quiet=quiet)
            ffmpeg.run(audio_out, overwrite_output=True, quiet=quiet)

    @staticmethod
    def merge_audio_video(
        video_path: str, audio_path: str, destination_name: Optional[str] = None
    ) -> Optional[str]:
        """Concat audio and video files."""
        try:
            basename = os.path.basename(video_path)
            extension = os.path.splitext(basename)[1]
            if not destination_name or destination_name == video_path:
                destination_name = basename.replace(extension, f"_swapped{extension}")
            output_file = os.path.join(os.path.dirname(video_path), destination_name)
            video_stream = ffmpeg.input(video_path)
            audio_stream = ffmpeg.input(audio_path)
            ffmpeg.concat(video_stream, audio_stream, v=1, a=1).output(output_file).run(
                overwrite_output=True, quiet=True
            )
            if destination_name == basename.replace(extension, f"_swapped{extension}"):
                shutil.move(
                    output_file, output_file.replace(f"_swapped{extension}", extension)
                )
                output_file = output_file.replace(f"_swapped{extension}", extension)
            return output_file
        except Error as err:
            print(err.stderr)
        return None

    def add_captions(self):
        video_clip = VideoFileClip(self.filepath).copy()
        txt_clip = (
            TextClip(
                self.caption,
                color="grey70",
                method="caption",
                fontsize=26,
                size=(self.width, ""),
                bg_color="grey20",
            )
            # .set_position(("center", "bottom"))
            .set_position(("center", "bottom")).set_duration(video_clip.duration)
        )
        result = CompositeVideoClip([video_clip, txt_clip])
        _, audio_extension = os.path.splitext(self.audio_path)
        tmp_audio_file = self.audio_path.replace(
            audio_extension, f".tmp{audio_extension}"
        )
        result_path = self.captioned_path
        result.write_videofile(
            result_path,
            temp_audiofile=tmp_audio_file,
            audio_codec=MOVIE_PY_CODEC_MAPPINGS[audio_extension]
            if audio_extension in MOVIE_PY_CODEC_MAPPINGS.keys()
            else "libmp3lame",
            ffmpeg_params=["-y"],
        )

    @property
    def captioned_path(self) -> str:
        _, _extension = os.path.splitext(self.filepath)
        return self.filepath.replace(_extension, f"{CAPTIONS_POST_FIX}{_extension}")

    @staticmethod
    def delete(element: VideoFile, folder: str = RESULTS):
        """Remove old results (previously generated video, audio, image files."""
        file_base, file_extension = os.path.splitext(element.filename)
        saved_meta = file_base + ".json"
        saved_meta_path = os.path.realpath(
            os.path.join(UPLOADS_DIR, folder, saved_meta)
        )
        if os.path.exists(saved_meta_path):
            os.unlink(saved_meta_path)
        video_path = os.path.realpath(os.path.join(UPLOADS_DIR, element.filename))
        if os.path.exists(video_path):
            os.unlink(video_path)
        if file_base.endswith(CAPTIONS_POST_FIX):  # pragma: nocover
            video_path = video_path.replace(CAPTIONS_POST_FIX, "")
        if os.path.exists(video_path):  # pragma: nocover
            os.unlink(video_path)
        if os.path.exists(element.audio_path.replace(CAPTIONS_POST_FIX, "")):
            os.unlink(element.audio_path.replace(CAPTIONS_POST_FIX, ""))
        if os.path.exists(element.audio_path):  # pragma: nocover
            os.unlink(element.audio_path)
        if os.path.exists(element.preview_path.replace(CAPTIONS_POST_FIX, "")):
            os.unlink(element.preview_path.replace(CAPTIONS_POST_FIX, ""))
        if os.path.exists(element.preview_path):  # pragma: nocover
            os.unlink(element.preview_path)
        if os.path.exists(element.muted_path.replace(CAPTIONS_POST_FIX, "")):
            os.unlink(element.muted_path.replace(CAPTIONS_POST_FIX, ""))
        if os.path.exists(element.muted_path):  # pragma: nocover
            os.unlink(element.muted_path)

    def save(self, folder: str = META, get_meta: bool = False):
        """Save this element's details/metadata to a json file."""
        file_base, file_extension = os.path.splitext(self.filename)
        save_name = file_base + ".json"
        if os.path.realpath(os.path.dirname(self.filepath)) != os.path.join(
            UPLOADS_DIR
        ):
            # new name/file, move the file to UPLOADS_DIR
            new_file_base = uuid.uuid4().hex
            save_name = new_file_base + ".json"
            new_filepath = os.path.join(UPLOADS_DIR, new_file_base + file_extension)
            shutil.move(self.filepath, new_filepath)
            if os.path.exists(self.audio_path):
                new_audio_path = os.path.join(
                    UPLOADS_DIR, self.audio.replace(file_base, new_file_base)
                )
                shutil.move(self.audio_path, new_audio_path)
            if os.path.exists(self.muted_path):
                new_muted_path = os.path.join(
                    UPLOADS_DIR, self.muted.replace(file_base, new_file_base)
                )
                shutil.move(self.muted_path, new_muted_path)
            if os.path.exists(self.preview_path):
                new_preview_path = os.path.join(
                    UPLOADS_DIR, self.preview.replace(file_base, new_file_base)
                )
                shutil.move(self.preview_path, new_preview_path)
            self.filepath = new_filepath
        if get_meta:
            self._get_properties(force=True, save=False)
        self._dict()
        base_dir = os.path.realpath(os.path.join(UPLOADS_DIR, folder))
        if not os.path.exists(base_dir):  # pragma: nocover
            os.makedirs(base_dir, mode=0o777)
        save_path = os.path.join(UPLOADS_DIR, folder, save_name)
        with open(save_path, "w") as f:
            f.write(orjson.dumps(self).decode())
