"""-*- coding: utf-8 -*-."""
from __future__ import annotations, absolute_import
import os
import uuid
import json

import orjson
from typing import Optional, Any, Dict, List, Union
import ffmpeg  # noqa
from PIL import Image, UnidentifiedImageError  # noqa

from .video_frames_split import VideoFramesSplit


class VideoFileInput(dict):
    id: str
    filepath: str
    title: str = "Unknown"
    frames: VideoFramesSplit = VideoFramesSplit({})
    preview_position: Optional[int]
    color: bool = True
    sound: bool = False
    caption: str = "Assess - Trust - Share"

    def __init__(self, dict_args: Dict[str, Any]):
        things: Dict[str, Any] = {}
        _filepath = dict_args.pop("filepath", None)
        if not _filepath:
            raise ValueError("Invalid file path")
        self.filepath = self.file_path(_filepath)
        self.id = dict_args.pop("id", uuid.uuid4().hex)
        self.preview_position = dict_args.pop("preview_position", 1)
        things["id"] = self.id
        things["filepath"] = self.filepath
        for key, val in dict_args.items():
            try:
                if key != "frames" and key != "filename":
                    self.__setattr__(key, val)
                    things[key] = val
                else:
                    _vals: List[VideoFramesSplit] = []
                    for entry in val:
                        _vals.append(VideoFramesSplit(entry))
                    self.__setattr__(key, _vals)
                    things[key] = _vals
            except AttributeError:
                pass
        super().__init__(things)

    @staticmethod
    def load_json(json_path: Union[str, os.PathLike]) -> Optional[dict]:
        """Load a json file's content."""
        try:
            content = open(json_path, "r").read()
            meta = orjson.loads(content)
            if not isinstance(meta, dict):
                raise TypeError("Invalid meta file")
            return meta
        except (orjson.JSONDecodeError, Exception) as error:
            try:
                content = open(json_path, "r").read()
                meta = json.loads(content)
                return meta
            except json.JSONDecodeError as err:
                print(error)
                print(err)
            return None

    @staticmethod
    def file_path(string: Optional[str]) -> str:
        if not string:
            raise FileNotFoundError("Invalid file path")
        _path: str = ""
        if os.path.exists(string):
            _path = str(os.path.realpath(string))
        elif os.path.exists(os.path.join(os.path.dirname(__file__), string)):
            _path = str(
                os.path.realpath(os.path.join(os.path.dirname(__file__), string))
            )
        if not _path:
            raise FileNotFoundError("Invalid file path")
        try:
            probe = ffmpeg.probe(_path)
            video = next(
                (
                    stream
                    for stream in probe["streams"]
                    if stream["codec_type"] == "video"
                ),
                None,
            )
            if not video:
                raise ValueError("Invalid video file (video not found in ffmpeg)")
        except ffmpeg.Error:
            raise ValueError("Invalid video file")
        try:
            with Image.open(_path) as img:
                img.verify()
            raise ValueError("Invalid file (image instead of video ?)")
        except UnidentifiedImageError:
            pass
        return _path
