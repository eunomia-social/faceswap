"""-*- coding: utf-8 -*-."""
from __future__ import annotations
from typing import Optional, Any, final
import math


def valid_int(possible: Any) -> Optional[int]:
    try:
        value = int(possible)
        if math.isfinite(value) and value > 0:
            return value
        raise ValueError("Only positive integers allowed")
    except (ValueError, Exception) as e:
        raise e


@final
class DimensionResize(dict):
    _start: Optional[int] = None
    _end: Optional[int] = None

    @staticmethod
    def from_string(string: str) -> DimensionResize:
        try:
            if "," not in string:
                # start nd end with same int (if valid)
                return DimensionResize(start=valid_int(string), end=valid_int(string))
            if (
                string.startswith(",")
                or string.startswith("null,")
                or string.startswith("None,")
            ):
                # ,200,  => start: null, end: 200 | null,200 => start: null, end: 200 | None,200 => start null, end: 200
                return DimensionResize(start=None, end=valid_int(string[1:]))
            if (
                string.endswith(",")
                or string.endswith(",null")
                or string.endswith(",None")
            ):
                # 200,  => start: 200, end: null
                return DimensionResize(start=valid_int(string[:-1]), end=None)
            parts = string.split(",")
            if len(parts) != 2:
                raise ValueError("Invalid input")
            start, end = parts
            return DimensionResize(start=valid_int(start), end=valid_int(end))
        except Exception as e:
            raise ValueError(str(e))

    def _slice(self) -> Any:
        return slice(self.start, self.end)

    @property
    def start(self) -> Optional[int]:
        return self._start

    @property
    def end(self) -> Optional[int]:
        return self._end

    def __init__(self, start: Optional[int] = None, end: Optional[int] = None):
        self._start = start
        self._end = end
        super(DimensionResize, self).__init__({"start": self._start, "end": self._end})

    def __call__(self, *args, **kwargs) -> Any:
        return self._slice()
