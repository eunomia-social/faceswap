"""-*- coding: utf-8 -*-."""
from __future__ import annotations, absolute_import
from enum import Enum
from typing import final, Any, Dict, Optional, List

from .dimension_resize import DimensionResize


class ModuleKey(int, Enum):
    five = 5
    ten = 10
    fifteen = 15


@final
class VideoFramesSplit(dict):
    x_resize: DimensionResize = DimensionResize()
    y_resize: DimensionResize = DimensionResize()
    z_resize: DimensionResize = DimensionResize()
    frame_start: int = 0
    frame_end: Optional[int] = None
    swap_index: List[int] = [1]
    modules_key: ModuleKey = ModuleKey.five

    def __init__(self, dict_args: Dict[str, Any]):
        things: Dict[str, Any] = {}
        for key, val in dict_args.items():
            try:
                if key not in {"x_resize", "y_resize", "z_resize", "modules_key"}:
                    self.__setattr__(key, val)
                    things[key] = val
                elif key == "modules_key":
                    if val == 10:
                        self.modules_key = ModuleKey.ten
                    elif val == 15:
                        self.modules_key = ModuleKey.fifteen
                    things[key] = self.modules_key
                else:
                    _val: Optional[DimensionResize] = None
                    if isinstance(val, tuple) and len(val) == 2:
                        _val = DimensionResize(start=val[0], end=val[1])
                    elif isinstance(val, dict) and "start" in val and "end" in val:
                        _val = DimensionResize(
                            start=val.get("start", None), end=val.get("end", None)
                        )
                    if _val is not None:
                        self.__setattr__(key, _val)
                        things[key] = {"start": _val.start, "end": _val.end}
                    else:
                        raise ValueError("Invalid resize args")
            except AttributeError:
                pass
        super().__init__(things)
