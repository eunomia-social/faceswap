"""-*- coding: utf-8 -*-."""
from __future__ import absolute_import
import os
import asyncio
from typing import Iterator, Tuple
import grpc
import datetime
import logging
import multiprocessing

try:  # noqa
    import reface_pb2 as models  # noqa
    import reface_pb2_grpc as rpc  # noqa
    from .lib import (  # noqa
        available_videos,
        upload_file,
        download_file,
        swap_faces,
        delete_item,
        job_info,
    )
    from .settings import RESULTS_DIR, CERT_PREFIX  # noqa
except ImportError:
    import sys

    sys.path.append(
        os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
    )  # noqa
    import grpc_server.reface_pb2 as models  # noqa
    import grpc_server.reface_pb2_grpc as rpc  # noqa
    from grpc_server.lib import (  # noqa
        available_videos,
        upload_file,
        download_file,
        swap_faces,
        delete_item,
        job_info,
    )  # noqa
    from grpc_server.settings import RESULTS_DIR, CERT_PREFIX  # noqa

_LOGGER = logging.getLogger(__name__)
_ONE_DAY = datetime.timedelta(days=1)
_PROCESS_COUNT = multiprocessing.cpu_count()
_THREAD_CONCURRENCY = _PROCESS_COUNT

try:
    _PORT = int(os.environ.get("REFACE_PORT", "5081"))
    _MAX_WORKERS = int(os.environ.get("REFACE_MAX_WORKERS", "10"))
except (ValueError, Exception):
    _PORT = 5081
    _MAX_WORKERS = _PROCESS_COUNT

_SOCKET = os.environ.get("REFACE_APP_SOCKET", f"[::]:{_PORT}")


class Servicer(rpc.RefaceServicer):
    async def get_videos(
        self, request: models.Empty, context: grpc.ServicerContext
    ) -> models.VideosListResponse:
        return await available_videos()

    def upload(
        self,
        request_iterator: Iterator[models.UploadRequest],
        context: grpc.ServicerContext,
    ) -> models.SwapResponse:
        return upload_file(request_iterator, context=context)

    def download(
        self, request: models.DownloadRequest, context: grpc.ServicerContext
    ) -> Iterator[models.Chunk]:
        return download_file(request.file, context=context)

    async def swap(
        self, request: models.SwapRequest, context: grpc.ServicerContext
    ) -> models.SwapResponse:
        return await swap_faces(
            image=request.image, video=request.video, context=context
        )

    async def delete(
        self, request: models.DeleteRequest, context: grpc.ServicerContext
    ) -> models.Empty:
        if request.Item and hasattr(request.Item, "name") and request.Item.name:
            if not delete_item(item=request.Item):
                await context.abort(  # type: ignore
                    code=grpc.StatusCode.NOT_FOUND,
                    details=f"job {request.JobId} not found",
                )
        else:
            os.makedirs(RESULTS_DIR, exist_ok=True)
            all_files = os.listdir(RESULTS_DIR)
            for item in all_files:
                delete_item(item=models.File(name=item), is_meta_item=True)
        return models.Empty()

    async def job_status(
        self, request: models.JobStatusRequest, context: grpc.ServicerContext
    ) -> models.JobStatusResponse:
        _status, enqueue_time, start_time, finish_time, result = await job_info(
            job_id=request.JobId
        )
        if _status == _status.not_found:
            await context.abort(  # type: ignore
                code=grpc.StatusCode.NOT_FOUND, details=f"job {request.JobId} not found"
            )
        return models.JobStatusResponse(
            status=_status.value,
            enqueue_time=enqueue_time,
            start_time=start_time,
            finish_time=finish_time,
            result=result,
        )


def load_secrets() -> Tuple[bytes, bytes, bytes]:
    with open(f"{CERT_PREFIX}server.key", "rb") as f:
        private_key = f.read()
    with open(f"{CERT_PREFIX}server.crt", "rb") as f:
        certificate_chain = f.read()
    with open(f"{CERT_PREFIX}client.crt", "rb") as f:
        root_certificates = f.read()
    return private_key, certificate_chain, root_certificates


async def main() -> None:
    """Start listening."""
    server = grpc.aio.server()  # noqa
    rpc.add_RefaceServicer_to_server(Servicer(), server)
    private_key, certificate_chain, root_certificates = load_secrets()
    server_credentials = grpc.ssl_server_credentials(
        [(private_key, certificate_chain)],
        root_certificates=root_certificates,
        require_client_auth=True,
    )
    server.add_secure_port(_SOCKET, server_credentials)
    await server.start()
    logging.info(f"Server started listening on {_SOCKET}")
    await server.wait_for_termination()


if __name__ == "__main__":  # pragma: no cover
    logging.basicConfig(level=logging.INFO)
    asyncio.run(main())
