"""-*- coding: utf-8 -*-."""
from __future__ import absolute_import
import os
import sys
from datetime import timedelta
import shutil
import tempfile
import uuid
from pathlib import Path
from typing import Optional, Iterator, Tuple
from hashlib import blake2b
import grpc
from PIL import Image  # noqa
from arq.jobs import Job, JobStatus

try:
    import reface_pb2 as models  # noqa
    from .video import VideoFile  # noqa
    from .settings import (  # noqa
        UPLOADS_DIR,
        RESULTS,
        META,
        RESULTS_DIR,
        META_DIR,
        CHUNK_SIZE,
        get_redis_pool,
    )
except ImportError:  # noqa
    sys.path.append(  # noqa
        os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
    )
    from grpc_server.video import VideoFile  # noqa
    from grpc_server.settings import (  # noqa
        UPLOADS_DIR,
        RESULTS,
        META,
        RESULTS_DIR,
        META_DIR,
        CHUNK_SIZE,
        get_redis_pool,
    )
    import grpc_server.reface_pb2 as models  # noqa


def _video_response(video_file: VideoFile) -> models.VideoResponse:
    return models.VideoResponse(
        id=video_file.id,
        video=models.File(name=video_file.filename),
        preview=video_file.preview,
        title=video_file.title,
        duration=video_file.duration,
        length=video_file.length,
        frame_rate=video_file.frame_rate,
        width=video_file.width,
        height=video_file.height,
        size=video_file.size,
        aspect_ratio=video_file.aspect_ratio,
        audio_bitrate=video_file.audio_bitrate,
        audio_codec=video_file.audio_codec,
        blurhash=video_file.blurhash,
    )


async def available_videos() -> models.VideosListResponse:
    os.makedirs(META_DIR, exist_ok=True)
    meta = (VideoFile(meta_file=_file) for _file in os.listdir(META_DIR))
    return models.VideosListResponse(videos=[_video_response(x) for x in meta])


def _upload_chunks(request: Iterator[models.UploadRequest]) -> Path:
    file_id = uuid.uuid4().hex
    file_extension = ".img"
    tmp_path = Path(os.path.join(tempfile.gettempdir(), f"{file_id}{file_extension}"))
    filename: Optional[str] = None
    with open(tmp_path, "wb") as f:
        for part in request:
            if not filename:
                filename = part.file.name
            f.write(part.data.buffer)
    if filename:
        _, file_extension = os.path.splitext(filename)
        if not file_extension:
            file_extension = ".img"
        shutil.move(
            tmp_path,
            Path(os.path.join(tempfile.gettempdir(), f"{file_id}{file_extension}")),
        )
        tmp_path = Path(
            os.path.join(tempfile.gettempdir(), f"{file_id}{file_extension}")
        )
    return tmp_path


def _save_uploaded_image(
    tmp_path: Path, context: grpc.ServicerContext
) -> Optional[models.File]:
    try:
        filename = os.path.split(tmp_path)[-1]
        _, file_extension = os.path.splitext(filename)
        upload_path = Path(os.path.join(UPLOADS_DIR, filename))
        with Image.open(tmp_path) as img:
            img.verify()
            image_format: Optional[str] = img.format
        if file_extension == ".img" and image_format is not None:
            upload_path = Path(
                os.path.join(
                    UPLOADS_DIR,
                    filename.replace(file_extension, f".{image_format.lower()}"),
                )
            )
        shutil.move(tmp_path, upload_path)
        return models.File(name=upload_path.name.split("/")[-1])
    except Exception as e:
        if os.path.exists(tmp_path):
            os.remove(tmp_path)
        context.abort(code=grpc.StatusCode.ABORTED, details=str(e))
        return None


def upload_file(
    request: Iterator[models.UploadRequest], context: grpc.ServicerContext
) -> Optional[models.File]:
    tmp_path = _upload_chunks(request)
    return _save_uploaded_image(tmp_path=tmp_path, context=context)


def download_file(file: models.File, context: grpc.ServicerContext):
    file_path = Path(os.path.join(UPLOADS_DIR, file.name))
    if not os.path.exists(file_path):
        context.abort(code=grpc.StatusCode.NOT_FOUND, details="File not found")
        return
    with open(file_path, "rb") as f:
        while True:
            piece = f.read(CHUNK_SIZE)
            if len(piece) == 0:
                return
            yield models.Chunk(buffer=piece)


def _delete_uploaded_file(file: models.File) -> bool:
    file_path = os.path.join(UPLOADS_DIR, file.name)
    if os.path.exists(file_path):
        os.unlink(file_path)
        return True
    return False


def delete_item(item: models.File, is_meta_item: bool = False) -> bool:
    if not is_meta_item:
        item_base, _ = os.path.splitext(item.name)
        item_json = f"{item_base}.json"
    else:
        item_json = item.name
    base_dir = RESULTS
    item_path = os.path.join(RESULTS_DIR, item_json)
    if not os.path.exists(item_path):
        base_dir = META
        item_path = os.path.join(META_DIR, item_json)
    if not os.path.exists(item_path):
        return _delete_uploaded_file(item)
    video_file = VideoFile(meta_file=item_path)
    VideoFile.delete(video_file, folder=base_dir)
    _delete_uploaded_file(item)
    return True


async def job_info(
    job_id: str,
) -> Tuple[
    JobStatus,
    Optional[str],
    Optional[str],
    Optional[str],
    Optional[models.VideoResponse],
]:
    redis = await get_redis_pool()
    job = Job(job_id=job_id, redis=redis)
    _status = await job.status()
    result = None
    enqueue_time = None
    start_time = None
    finish_time = None
    if not _status == _status.not_found:
        info = await job.info()
        enqueue_time = str(info.enqueue_time) if info else None
    if _status == JobStatus.complete:
        _file_name = await job.result(timeout=5)
        if _file_name:
            _file_id, _ = os.path.splitext(_file_name)
            _results_meta = os.path.join(RESULTS_DIR, f"{_file_id}.json")
            if os.path.exists(_results_meta):
                video_file = VideoFile(meta_file=_results_meta)
                result = _video_response(video_file=video_file)
    result_info = await job.result_info()
    if result_info:
        start_time = str(result_info.start_time)
        finish_time = str(result_info.finish_time)
    redis.close()
    await redis.wait_closed()
    return _status, enqueue_time, start_time, finish_time, result


async def swap_faces(
    image: models.File, video: models.File, context: grpc.ServicerContext
) -> models.SwapResponse:
    """Get a combined video with the provided `image` and `video`.

    Loads the image and the video files,
    does the ml stuff
    returns the new modified video."""

    video_file = os.path.split(video.name)[1]
    image_file = os.path.split(image.name)[1]
    file_base, file_extension = os.path.splitext(video_file)
    video_path = os.path.join(UPLOADS_DIR, video_file)
    image_path = os.path.realpath(os.path.join(UPLOADS_DIR, image_file))
    if not os.path.isfile(video_path) or not os.path.isfile(image_path):
        thing = "Image" if os.path.isfile(video_path) else "Video"
        await context.abort(  # type: ignore
            code=grpc.StatusCode.NOT_FOUND,
            details=f"{thing} file not found",
        )
    meta_file = os.path.realpath(os.path.join(META_DIR, f"{file_base}.json"))
    video_item = VideoFile(meta_file=meta_file)
    if os.path.exists(video_item.filepath) and os.path.exists(video_item.muted_path):
        job_key = f"{image_file}_{video_file}"
        job_id = blake2b(job_key.encode(), digest_size=24).hexdigest()
        redis = await get_redis_pool()
        await redis.enqueue_job(
            "reface_task",
            _job_id=job_id,
            _expires=timedelta(minutes=30),
            image_file=image_file,
            video_file=video_file,
            meta_file=meta_file,
        )
        redis.close()
        await redis.wait_closed()
        return models.SwapResponse(JobId=job_id)
    await context.abort(  # type: ignore
        code=grpc.StatusCode.NOT_FOUND,
        details=f"Audio file not found",
    )
