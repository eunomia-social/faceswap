"""-*- coding: utf-8 -*-."""
import os
import torch
from arq.connections import create_pool, RedisSettings

USE_CPU = not torch.cuda.is_available()
REDIS_HOST = os.environ.get("REDIS_HOST", "127.0.0.1")
REDIS_PORT = int(os.environ.get("REDIS_PORT", "7777"))
CHUNK_SIZE = 1024 * 1024  # 1MB
UPLOADS_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), "..", "files"))
if os.environ.get("APP_ENV", "production") == "testing":
    UPLOADS_DIR = os.path.realpath(
        os.path.join(os.path.dirname(__file__), "tests", "data", "files")
    )

if not os.path.exists(UPLOADS_DIR):  # pragma: nocover
    # not in docker
    UPLOADS_DIR = os.path.realpath(
        os.path.join(os.path.dirname(__file__), "..", "..", "..", "files")
    )
RESULTS = "results"
META = "meta"
CAPTIONS_POST_FIX = "becc496ac9a684d5"
CERT_PREFIX = os.environ.get("CERT_PREFIX", "")
if not os.path.exists(
    os.path.join(os.path.dirname(__file__), f"{CERT_PREFIX}server.crt")
):
    cert_root = os.path.realpath(
        os.path.join(os.path.dirname(__file__), "..", "..", "..", "certs")
    )
    if os.environ.get("APP_ENV", "production") == "testing":
        cert_root = os.path.realpath(
            os.path.join(os.path.dirname(__file__), "tests", "data", "certs")
        )
    CERT_PREFIX = str(cert_root) + "/" + CERT_PREFIX

RESULTS_DIR = os.path.realpath(os.path.join(UPLOADS_DIR, RESULTS))
META_DIR = os.path.realpath(os.path.join(UPLOADS_DIR, META))
AUDIO_MAPPINGS = {
    "aac": "m4a",
    "flac": "flac",
    "mp3": "mp3",
    "opus": "opus",
    "vorbis": "ogg",
}
MOVIE_PY_CODEC_MAPPINGS = {
    ".m4a": "aac",
    ".mp3": "mp3",
    ".ogg": "vorbis",
}


async def get_redis_pool():
    if os.environ.get("APP_ENV", "production") == "testing":
        from redislite import Redis  # noqa

        db_path = "/tmp/redis.db"
        if os.path.exists(db_path):  # pragma: nocover
            os.remove(db_path)
        rdb = Redis(db_path)
        socket_address = f"//{rdb.socket_file}"
        redis_settings = RedisSettings(socket_address=socket_address)
    else:  # pragma: nocover
        redis_settings = RedisSettings(
            host=REDIS_HOST,
            port=REDIS_PORT,
        )
    return await create_pool(redis_settings)
