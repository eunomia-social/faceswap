"""-*- coding: utf-8 -*-."""
from __future__ import absolute_import
import os
import sys
import traceback
import uuid
import logging
from typing import Dict, Any, Tuple, Union, List
import torch
import numpy as np
import imageio  # noqa
from skimage.transform import resize  # noqa
from skimage import img_as_ubyte  # noqa
import cv2  # noqa


try:  # noqa
    from settings import UPLOADS_DIR, RESULTS, CAPTIONS_POST_FIX  # noqa
    from video import VideoFile, DimensionResize, VideoFramesSplit  # noqa
    from utils.part_swap import load_checkpoints, make_video, PartSwapGenerator  # noqa
    from utils.sync_batchnorm import DataParallelWithCallback  # noqa
    from utils.modules.segmentation_module import SegmentationModule  # noqa
except ImportError:  # pragma: nocover
    sys.path.append(
        os.path.realpath(os.path.join(os.path.dirname(__file__), "..", ".."))
    )
    from grpc_server.settings import (  # noqa
        UPLOADS_DIR,  # noqa
        RESULTS,  # noqa
        CAPTIONS_POST_FIX,  # noqa
    )
    from grpc_server.video import VideoFile, DimensionResize, VideoFramesSplit  # noqa
    from grpc_server.utils.part_swap import (  # noqa
        load_checkpoints,
        make_video,
        PartSwapGenerator,
    )  # noqa
    from grpc_server.utils.sync_batchnorm import DataParallelWithCallback  # noqa
    from grpc_server.utils.modules.segmentation_module import SegmentationModule  # noqa

USE_CPU = not torch.cuda.is_available()

logger = logging.getLogger("arq.worker")


def _load_checkpoints(key: int):
    try:
        return load_checkpoints(
            config=os.path.realpath(
                os.path.join(
                    os.path.dirname(__file__),
                    "..",
                    "utils",
                    "config",
                    f"vox-256-sem-{key}segments.yaml",
                )
            ),
            checkpoint=os.path.realpath(
                os.path.join(
                    os.path.dirname(__file__),
                    "..",
                    "..",
                    "models",
                    f"vox-{key}segments.pth.tar",
                )
            ),
            blend_scale=1,
            cpu=USE_CPU,
        )

    except RuntimeError:  # pragma: nocover
        return load_checkpoints(
            config=os.path.realpath(
                os.path.join(
                    os.path.dirname(__file__),
                    "..",
                    "utils",
                    "config",
                    f"vox-256-sem-{key}segments.yaml",
                )
            ),
            checkpoint=os.path.realpath(
                os.path.join(
                    os.path.dirname(__file__),
                    "..",
                    "..",
                    "models",
                    f"vox-{key}segments.pth.tar",
                )
            ),
            blend_scale=1,
            cpu=True,
        )


reconstruction_module5, segmentation_module5 = _load_checkpoints(5)
reconstruction_module10, segmentation_module10 = _load_checkpoints(10)
reconstruction_module15, segmentation_module15 = _load_checkpoints(15)


def to_grayscale(img: np.ndarray) -> np.ndarray:
    """RGB image to grayscale."""
    gray = cv2.cvtColor(img.astype("float32"), cv2.COLOR_BGR2GRAY)  # noqa
    img2 = np.zeros_like(img)
    img2[:, :, 0] = gray
    img2[:, :, 1] = gray
    img2[:, :, 2] = gray
    return img2


def _reface_results(
    video_item: VideoFile,
    output_path: str,
) -> str:
    _output_path = output_path
    if os.path.exists(video_item.audio_path):
        final_output = video_item.merge_audio_video(
            video_path=output_path,
            audio_path=video_item.audio_path,
        )
        if final_output:
            # get thumbnail, framerate, size etc...
            _output_path = final_output
    output_video = VideoFile(
        dict_args={
            "filepath": _output_path,
            "caption": video_item.caption
        }
    )
    output_video.add_captions()
    _, output_extension = os.path.splitext(output_video.filename)
    result_video = VideoFile(
        {
            "filename": output_video.filename.replace(
                output_extension, f"{CAPTIONS_POST_FIX}{output_extension}"
            ),
            "title": video_item.title,
        },
        force_meta=True,
    )
    # save the results metadata so we won't need to calculate them again later
    result_video.save(RESULTS)
    return result_video.filename


def _get_modules(
    module_key: int,
) -> Tuple[
    Union[DataParallelWithCallback, PartSwapGenerator],
    Union[DataParallelWithCallback, SegmentationModule],
]:
    if module_key == 5:
        return segmentation_module5, reconstruction_module5
    elif module_key == 10:
        return segmentation_module10, reconstruction_module10
    return segmentation_module15, reconstruction_module15


def _reface_batch(
    video_part: VideoFramesSplit, target_video: np.ndarray, source_image: np.ndarray
) -> List[np.ndarray]:
    x1 = video_part.x_resize.start
    x2 = video_part.x_resize.end
    y1 = video_part.y_resize.start
    y2 = video_part.y_resize.end
    start_frame = video_part.frame_start
    end_frame = video_part.frame_end
    target = [
        resize(frame[x1:x2, y1:y2,:],(256, 256))[..., :3]
        for frame in target_video[start_frame:end_frame]  # noqa
    ]
    segmentation_module, reconstruction_module = _get_modules(
        video_part.modules_key.value
    )
    _predictions = make_video(
        swap_index=video_part.swap_index,
        source_image=source_image,
        target_video=target,
        reconstruction_module=reconstruction_module,
        segmentation_module=segmentation_module,
        cpu=USE_CPU,
    )
    video_new = [
        resize(frame,(int(x2-x1), int(y2-y1)))[..., :3]
        for frame in _predictions
    ]
    video_out = []
    for i, _frame in zip(target_video[start_frame:end_frame], video_new):
        i = img_as_ubyte(i)
        i[x1:x2, y1:y2,:3] = img_as_ubyte(_frame)
        video_out.append(i)
    return video_out


async def reface_task(
    ctx: Dict[str, Any], image_file: str, video_file: str, meta_file: str  # noqa
):
    """Reface background task."""
    image_path = os.path.realpath(os.path.join(UPLOADS_DIR, image_file))
    try:
        _, file_extension = os.path.splitext(video_file)
        video_path = os.path.realpath(os.path.join(UPLOADS_DIR, video_file))
        video_item = VideoFile(meta_file=meta_file)
        source_image = imageio.imread(image_path)
        target_video = imageio.mimread(video_path, memtest="1024MB")
        source_image = resize(source_image, (256, 256))[..., :3]
        if not video_item.color:
            source_image = to_grayscale(source_image)
        output_path = os.path.realpath(
            os.path.join(UPLOADS_DIR, f"{uuid.uuid4().hex}{file_extension}")
        )
        partials: List[List[np.ndarray]] = []
        output_frames: List[np.ndarray] = []
        for video_part in video_item.frames:
            predictions = _reface_batch(
                video_part=video_part,
                target_video=target_video,
                source_image=source_image,
            )
            partials.append(predictions)
        for partial in partials:
            if not output_frames:
                output_frames = partial
            else:
                output_frames += partial
        imageio.mimsave(
            output_path,
            [img_as_ubyte(frame) for frame in output_frames],
            fps=int(video_item.frame_rate) if video_item.frame_rate else 25,
        )
        # delete the source image (we don't need it anymore)
        if os.path.exists(image_path):
            os.unlink(image_path)
        return _reface_results(
            video_item=video_item,
            output_path=output_path,
        )
    except Exception as error:  # pragma: nocover
        if os.path.exists(image_path):
            os.unlink(image_path)
        print("reface_task():")
        traceback.format_exc()
        logger.critical(error)
        return None
