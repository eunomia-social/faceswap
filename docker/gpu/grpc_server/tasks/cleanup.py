"""-*- coding: utf-8 -*-."""
from __future__ import absolute_import
from typing import Dict, Any
import os
import sys
import time

try:  # noqa
    from settings import UPLOADS_DIR, META, RESULTS  # noqa
    from lib import delete_item  # noqa
    from reface_pb2 import File  # noqa
except ImportError:  # pragma: nocover
    sys.path.append(
        os.path.realpath(os.path.join(os.path.dirname(__file__), "..", ".."))
    )
    from grpc_server.settings import (  # noqa
        UPLOADS_DIR,  # noqa
        RESULTS,  # noqa
        META,  # noqa
    )  # noqa
    from grpc_server.lib import delete_item  # noqa
    from grpc_server.reface_pb2 import File  # noqa


async def cleanup_task(ctx: Dict[str, Any]):
    """Delete 'old' results and user uploaded files if any."""
    meta_files = os.listdir(os.path.join(UPLOADS_DIR, META))
    idz_to_keep = [os.path.splitext(item)[0] for item in meta_files]
    results_meta = os.listdir(os.path.join(UPLOADS_DIR, RESULTS))
    result_idz = [os.path.splitext(item)[0] for item in results_meta]
    for result_json in results_meta:
        complete_path = os.path.join(UPLOADS_DIR, RESULTS, result_json)
        # file created at least 10 minutes from now
        if (
            abs(time.time() - os.stat(complete_path).st_ctime) > 10 * 60
        ) or os.environ.get("APP_ENV", "production") == "testing":
            delete_item(item=File(name=str(result_json)), is_meta_item=True)
    for item in os.listdir(UPLOADS_DIR):
        complete_path = os.path.join(UPLOADS_DIR, item)
        if os.path.isdir(complete_path):
            continue
        item_id = os.path.splitext(item)[0]
        if item_id not in result_idz and item_id not in idz_to_keep and not item_id.endswith("_no_audio"):
            os.remove(complete_path)
