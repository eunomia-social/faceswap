"""-*- coding: utf-8 -*-."""
import os
import sys
from datetime import datetime
from typing import Any, Dict

from arq import cron, run_worker
from arq.connections import RedisSettings

try:
    from .tasks import reface_task, cleanup_task
except ImportError:
    sys.path.append(os.path.realpath(os.path.join(os.path.dirname(__file__), "..")))
    from grpc_server.tasks import reface_task, cleanup_task  # type: ignore
except RuntimeError:
    import torch

    if (
        torch.cuda.is_available()
        and os.environ.get("APP_ENV", "production") != "production"
    ):
        os.environ["CUDA_NO_MEMORY"] = "true"
        try:  # noqa
            from .tasks import reface_task, cleanup_task  # noqa
        except ImportError:
            sys.path.append(
                os.path.realpath(os.path.join(os.path.dirname(__file__), ".."))
            )
            from grpc_server.tasks import reface_task, cleanup_task  # type: ignore


class WorkerSettings:
    """
    Settings for the ARQ worker.
    """

    if os.environ.get("APP_ENV", "production") == "testing":
        from redislite import Redis  # noqa

        db_path = "/tmp/redis.db"
        if os.path.exists(db_path):
            os.remove(db_path)
        rdb = Redis(db_path)
        socket_address = f"//{rdb.socket_file}"
        redis_settings = RedisSettings(socket_address=socket_address)
    else:
        redis_settings = RedisSettings(
            host=os.environ.get("REDIS_HOST", "redis"),
            port=int(os.environ.get("REDIS_PORT", "7777")),
        )
    allow_abort_jobs = True
    cron_jobs = [cron(cleanup_task, name="cleanup_task", hour=23, minute=59, second=59)]  # type: ignore
    job_timeout = 10 * 60
    keep_result = 10 * 60
    functions = [reface_task]
