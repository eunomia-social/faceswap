syntax = "proto3";

// inputs
message Empty {}
message Chunk {
  bytes buffer = 1;
}
message File {
  string name = 1;
}
/* Represents the upload request inputs. */
message UploadRequest {
  Chunk data = 1; // the file's bytes
  File file = 2; // the name of the file to upload
}
/* Represents the download request inputs. */
message DownloadRequest {
  File file = 1; // the file to download
}
/* Represents the swap request inputs. */
message SwapRequest {
  File image = 1; // the image file to use
  File video = 2; // the video file to use
}
/* Represents the job status request input. */
message JobStatusRequest{
  string JobId = 1;
}
/* Represents the file deletion request input. */
message DeleteRequest {
  optional File Item = 1;
}
/* Represents the swap response. */
message SwapResponse {
  string JobId = 1; // the id of the queued job
}
// outputs
/* Represents the video details response. */
message VideoResponse{
  string id = 1;
  File video = 2; // the video's file name
  string title = 3; // the title of the video
  string preview = 4; // the image thumbnail(preview) of the video
  float duration = 5; // the duration of the video
  string length = 6; // the duration of the video as string
  string frame_rate = 7; // the video's frame rate
  int32 width = 8; // the video's width
  int32 height = 9; // the video's height
  string size = 10; // the video's width-x-height string
  float aspect_ratio = 11; // the width/height aspect ratio
  optional string audio_bitrate = 12; // the video's audio bitrate
  optional string audio_codec = 13; // the video's audio_codec
  optional string blurhash = 14; // the video's blurhash
}
/* Represents the list of videos response. */
message VideosListResponse {
  repeated VideoResponse videos = 1; // the list of video files and titles
}
/* Represents the job status response. */
message JobStatusResponse {
    string status =  1; // the status of the job
    string enqueue_time = 2; // when the job was enqueued
    optional string start_time = 3; // when the job started
    optional string finish_time = 4; // when the job finished
    optional VideoResponse result = 5; // the video result of the job
}

// endpoints
service Reface {
  rpc get_videos(Empty) returns (VideosListResponse);
  rpc upload(stream UploadRequest) returns (File);
  rpc download(DownloadRequest) returns (stream Chunk);
  rpc delete(DeleteRequest) returns (Empty);
  rpc swap(SwapRequest) returns(SwapResponse);
  rpc job_status(JobStatusRequest) returns(JobStatusResponse);
}
