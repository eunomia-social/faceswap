const webpack = require('webpack');
const { env } = require('process');
const { join, resolve } = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const AssetsManifestPlugin = require('webpack-assets-manifest');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const rules = require('./rules');

const basePath = resolve(join(__dirname, '..'));
const mainPath = resolve(join(basePath, 'src', 'main.js'));
const stylesPath = resolve(join(basePath, 'src', 'styles', 'main.scss'));


const environment = {
  NODE_ENV: env.NODE_ENV,
  PUBLIC_OUTPUT_PATH: /static/,
};

module.exports = {
  entry: {
    main: mainPath,
    styles: stylesPath,
  },
  output: {
    filename: 'js/[name]-[chunkhash].js',
    chunkFilename: 'js/[name]-[chunkhash].chunk.js',
    hotUpdateChunkFilename: 'js/[id]-[hash].hot-update.js',
    path: resolve(join(basePath, 'public', 'static')),
    publicPath: '/static/',
  },

  optimization: {
    runtimeChunk: {
      name: 'common',
    },
    splitChunks: {
      cacheGroups: {
        default: false,
        vendors: false,
        common: {
          name: 'common',
          chunks: 'all',
          minChunks: 2,
          minSize: 0,
          test: /^(?!.*[\\\/]node_modules[\\\/]react-intl[\\\/]).+$/,
        },
      },
    },
    occurrenceOrder: true,
  },

  module: {
    rules: Object.keys(rules).map(key => rules[key]),
  },

  plugins: [
    new webpack.EnvironmentPlugin(JSON.parse(JSON.stringify(environment))),
    new webpack.NormalModuleReplacementPlugin(
      /^history\//, (resource) => {
        resource.request = resource.request.replace(/^history/, 'history/es');
      },
    ),
    new MiniCssExtractPlugin({
      filename: 'css/[name]-[contenthash:8].css',
      chunkFilename: 'css/[name]-[contenthash:8].chunk.css',
    }),
    new AssetsManifestPlugin({
      integrity: true,
      integrityHashes: ['sha256'],
      entrypoints: true,
      writeToDisk: true,
      publicPath: true,
    }),
    new HtmlWebpackPlugin({
      inject: 'head',
      alwaysWriteToDisk: true,
      filename: '../index.html',
      publicPath: '/static/',
      template: resolve('public', '_index.html'),
    }),
    new HtmlWebpackHarddiskPlugin(),
  ],

  resolve: {
    extensions: [
      '.js',
      '.scss',
      '.css',
      '.png'
    ],
    modules: [
      resolve('src'),
      'node_modules',
    ],
  },

  resolveLoader: {
    modules: ['node_modules'],
  },

  node: {
    Buffer: true,
    fs: "empty"
  },
};
