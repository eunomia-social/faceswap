const { resolve } = require('path');
const { merge } = require('webpack-merge');
const sharedConfig = require('./shared');

module.exports = merge(sharedConfig, {
    mode: 'development',
    cache: true,
    devtool: 'cheap-module-eval-source-map',
  
    stats: {
      errorDetails: true,
    },
  
    output: {
      pathinfo: true,
    },
  
    devServer: {
      clientLogLevel: 'none',
      compress: true,
      quiet: false,
      disableHostCheck: true,
      host: 'localhost',
      port: 3000,
      https: false,
      hot: false,
      contentBase: resolve('public'),
      inline: true,
      useLocalIp: false,
      public: 'localhost:3000',
      publicPath: 'static',
      historyApiFallback: {
        disableDotRule: true,
      },
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      overlay: true,
      stats: {
        entrypoints: false,
        errorDetails: false,
        modules: false,
        moduleTrace: false,
      },
      watchOptions: {
        ignored: '**/node_modules/**'
      },
      writeToDisk: true,
    },
  });