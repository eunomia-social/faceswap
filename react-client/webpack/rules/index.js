const babel = require('./babel');
const css = require('./css');
const file = require('./file');
const nodeModules = require('./node_modules');

module.exports = {
  file,
  css,
  nodeModules,
  babel,
};
