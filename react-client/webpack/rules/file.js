const { join } = require('path');

const static_assets_extensions = [
  '.jpg',  '.gif',
  '.jpeg', '.png',
  '.tiff', '.ico',
  '.svg',  '.eot',
  '.otf',  '.ttf',
  '.woff', '.woff2',
];

module.exports = {
  test: new RegExp(`(${static_assets_extensions.join('|')})$`, 'i'),
  use: [
    {
      loader: 'file-loader',
      options: {
        name(file) {
          if (file.includes('src')) {
            return '/media/[path][name]-[hash].[ext]';
          }
          return '/media/[folder]/[name]-[hash:8].[ext]';
        },
        context: join('src'),
      },
    },
  ],
};
