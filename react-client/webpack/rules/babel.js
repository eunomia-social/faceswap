const { join, resolve } = require('path');
const { env } = require('process');

module.exports = {
  test: /\.(js|jsx|mjs)$/,
  include: [
    'src',
  ].map(p => resolve(p)),
  exclude: /node_modules/,
  use: [
    {
      loader: 'babel-loader',
      options: {
        cacheDirectory: 'tmp/cache/babel-loader',
        cacheCompression: env.NODE_ENV === 'production',
        compact: env.NODE_ENV === 'production',
      },
    },
  ],
};
