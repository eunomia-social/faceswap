import React from 'react';
import { Provider } from 'react-redux';
import { IntlProvider, addLocaleData } from 'react-intl';
import { BrowserRouter, Route } from 'react-router-dom';

import configureStore from './external/store';
import UI from './external/ui';
import { getLocale } from "./locales";

import Storage from './storage';
import RefaceSetup from "./intro";


const { localeData, messages } = getLocale();
addLocaleData(localeData);
const store  = configureStore();

export const SessionStore = new Storage();

class AppMount extends React.PureComponent {
    render () {
        if (!SessionStore.refaceMastodonToken || !SessionStore.refaceMastodonUrl || !SessionStore.refaceUrl) {
            return <RefaceSetup />
        }
        return (
            <IntlProvider locale={'en'} messages={messages}>
                <Provider store={store}>
                    <BrowserRouter basename='/eunomia'>
                        <Route path='/' component={UI} />
                    </BrowserRouter>
                </Provider>
            </IntlProvider>
        );
    }
}

export default AppMount;
