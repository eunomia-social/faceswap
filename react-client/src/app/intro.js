import React from 'react';
import {SessionStore} from "./index";

export default class RefaceSetup extends React.PureComponent {

    state = {
        domain: '',
        token: '',
        refaceUrl: ''
    }
    render() {
        return (
            <div style={{ display: 'flex', flexDirection: 'column' }}>
                <div>
                <label htmlFor="reface-url-input">
                    Reface Url:&nbsp;&nbsp;
                </label>
                <input
                    id="reface-url-input"
                    type="text"
                    onChange={(e) => this.setState({refaceUrl: e.target.value})}
                    placeholder="https://reface.url"
                    value={this.state.refaceUrl}
                /><br /><br />
                </div>
                <div>
                <label htmlFor="domain-input">
                    Mastodon Url:&nbsp;&nbsp;
                </label>
                <input
                    id="domain-input"
                    type="text"
                    onChange={(e) => this.setState({domain: e.target.value})}
                    placeholder="https://one.eunomia.instance"
                    value={this.state.domain}
                /><br /><br />
                </div>
                <div>
                <label htmlFor="token-input">
                    Mastodon Token:&nbsp;&nbsp;
                </label>
                <input
                    id="token-input"
                    type="text"
                    placeholder="the-mastodon-token"
                    onChange={(e) => this.setState({token: e.target.value})}
                    value={this.state.token}
                /><br /><br />
                </div>
                <button type="button" onClick={() => {
                    SessionStore.refaceMastodonUrl = this.state.domain;
                    SessionStore.refaceMastodonToken = this.state.token;
                    SessionStore.refaceUrl = this.state.refaceUrl;
                    window.location.reload();
                }}>Save</button>
            </div>
        );
    }
}
