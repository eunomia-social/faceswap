let _refaceAccessToken = undefined;
let _refaceMastodonUrl = undefined;
let _refaceUrl = undefined;

export default class Storage {
    constructor() {}

    get refaceMastodonToken() {
        if (_refaceAccessToken !== undefined) {
            return _refaceAccessToken;
        }
        return window.sessionStorage.getItem('refaceMastodonToken');
    }
    set refaceMastodonToken(value) {
        _refaceAccessToken = value;
        window.sessionStorage.setItem('refaceMastodonToken', value);
    }
    get refaceMastodonUrl() {
        if (_refaceMastodonUrl !== undefined) {
            return _refaceMastodonUrl;
        }
        return window.sessionStorage.getItem('refaceMastodonUrl');
    }
    set refaceMastodonUrl(value) {
        _refaceMastodonUrl = value;
        window.sessionStorage.setItem('refaceMastodonUrl', value);
    }
    get refaceUrl() {
        if (_refaceUrl !== undefined) {
            return _refaceUrl;
        }
        return window.sessionStorage.getItem('refaceUrl');
    }
    set refaceUrl(value) {
        _refaceUrl = value;
        window.sessionStorage.setItem('refaceUrl', value);
    }
}
