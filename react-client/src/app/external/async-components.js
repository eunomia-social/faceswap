export function RefaceVideos() {
  return import(/* webpackChunkName: "reface_videos" */'../reface/videos');
}

export function RefaceUser() {
  return import(/* webpackChunkName: "reface_user" */'../reface/user');
}

export function RefaceResults() {
  return import(/* webpackChunkName: "reface_results" */'../reface/results');
}
