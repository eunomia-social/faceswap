import { createStore, applyMiddleware, compose } from 'redux';
import { combineReducers } from 'redux-immutable';
import { Map as ImmutableMap, List } from 'immutable';
import thunk from 'redux-thunk';

const initialState = ImmutableMap({
    dummy: true,
    columns: List([]),
});

function dummyReducer (state = initialState, action) {
    if (action.type === "changeDummy") {
        state.set('dummy', !state.get('dummy', false));
    }
    return state;
}

const appReducer = combineReducers([dummyReducer]);

const store = createStore( appReducer, compose(applyMiddleware(
    thunk
)));

export default () => store;
