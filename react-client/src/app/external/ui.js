import React from 'react';
import PropTypes from 'prop-types';
import { Redirect, withRouter } from 'react-router-dom';
import classNames from 'classnames';
import { debounce } from 'lodash';
import { injectIntl } from 'react-intl';
import { connect } from 'react-redux';

import ColumnsAreaContainer from './columns_area_container';
import { WrappedSwitch, WrappedRoute } from './react_router_helpers';
import {
    RefaceVideos,
    RefaceUser,
    RefaceResults,
} from './async-components';
import {List} from "immutable";


const mapStateToProps = (state) => ({ columns: state.get('columns', List([])), layout: 'layout-single-column'});

class SwitchingColumnsArea extends React.PureComponent {

    static propTypes = {
        children: PropTypes.node,
        location: PropTypes.object,
        mobile: PropTypes.bool,
    };

    UNSAFE_componentWillMount() {
        if (this.props.mobile) {
            document.body.classList.toggle('layout-single-column', true);
            document.body.classList.toggle('layout-multiple-columns', false);
        } else {
            document.body.classList.toggle('layout-single-column', false);
            document.body.classList.toggle('layout-multiple-columns', true);
        }
    }

    componentDidUpdate(prevProps) {
        if (prevProps.mobile !== this.props.mobile) {
            document.body.classList.toggle('layout-single-column', this.props.mobile);
            document.body.classList.toggle('layout-multiple-columns', !this.props.mobile);
        }
    }

    setRef = c => {
        if (c) {
            this.node = c.getWrappedInstance();
        }
    }

    shouldUpdateScroll (_, { location }) {
        return false;
    }

    render() {
        const {children, mobile} = this.props;
        const redirect = <Redirect from='/' to='/reface' exact/>;
        return (
            <ColumnsAreaContainer ref={this.setRef} singleColumn={mobile}>
                <WrappedSwitch>
                    {redirect}
                    <WrappedRoute path='/reface' exact component={RefaceVideos} content={children} componentParams={{ shouldUpdateScroll: this.shouldUpdateScroll }} />
                    <WrappedRoute path='/reface/photo' exact component={RefaceUser} content={children} componentParams={{ shouldUpdateScroll: this.shouldUpdateScroll }} />
                    <WrappedRoute path='/reface/results' exact component={RefaceResults} content={children} componentParams={{ shouldUpdateScroll: this.shouldUpdateScroll }} />
                </WrappedSwitch>
            </ColumnsAreaContainer>
        );
    }

}

export default @connect(mapStateToProps)
@injectIntl
@withRouter
class UI extends React.PureComponent {

    static contextTypes = {
        router: PropTypes.object.isRequired,
    };

    static propTypes = {
        dispatch: PropTypes.func.isRequired,
        children: PropTypes.node,
        isComposing: PropTypes.bool,
        hasComposingText: PropTypes.bool,
        hasMediaAttachments: PropTypes.bool,
        canUploadMore: PropTypes.bool,
        location: PropTypes.object,
        intl: PropTypes.object.isRequired,
        dropdownMenuIsOpen: PropTypes.bool,
        layout: PropTypes.string.isRequired,
        firstLaunch: PropTypes.bool,
    };

    state = {
        draggingOver: false,
    };

    handleDragEnter = (e) => {
        e.preventDefault();

        if (!this.dragTargets) {
            this.dragTargets = [];
        }

        if (this.dragTargets.indexOf(e.target) === -1) {
            this.dragTargets.push(e.target);
        }

        if (e.dataTransfer && Array.from(e.dataTransfer.types).includes('Files') && this.props.canUploadMore) {
            this.setState({ draggingOver: true });
        }
    }

    handleDragOver = (e) => {
        if (this.dataTransferIsText(e.dataTransfer)) return false;

        e.preventDefault();
        e.stopPropagation();

        try {
            e.dataTransfer.dropEffect = 'copy';
        } catch (err) {

        }

        return false;
    }

    handleDragLeave = (e) => {
        e.preventDefault();
        e.stopPropagation();

        this.dragTargets = this.dragTargets.filter(el => el !== e.target && this.node.contains(el));

        if (this.dragTargets.length > 0) {
            return;
        }

        this.setState({ draggingOver: false });
    }

    dataTransferIsText = (dataTransfer) => {
        return (dataTransfer && Array.from(dataTransfer.types).filter((type) => type === 'text/plain').length === 1);
    }

    closeUploadModal = () => {
        this.setState({ draggingOver: false });
    }

    handleServiceWorkerPostMessage = ({ data }) => {
        if (data.type === 'navigate') {
            this.context.router.history.push(data.path);
        } else {
            console.warn('Unknown message type:', data.type);
        }
    }

    handleLayoutChange = debounce(() => {
        this.props.dispatch(clearHeight()); // The cached heights are no longer accurate, invalidate
    }, 500, {
        trailing: true,
    });

    handleResize = () => {
        const layout = layoutFromWindow();

        if (layout !== this.props.layout) {
            this.handleLayoutChange.cancel();
            this.props.dispatch(changeLayout(layout));
        } else {
            this.handleLayoutChange();
        }
    }

    componentDidMount () {
        window.addEventListener('resize', this.handleResize, { passive: true });
    }

    componentWillUnmount () {
        window.removeEventListener('resize', this.handleResize);
    }

    setRef = c => {
        this.node = c;
    }

    render () {
        const { children, isComposing, location, dropdownMenuIsOpen, layout } = this.props;
        return (
            <div className={classNames('ui', { 'is-composing': isComposing })} ref={this.setRef} style={{ pointerEvents: dropdownMenuIsOpen ? 'none' : null }}>
                <SwitchingColumnsArea location={location} mobile={layout === 'mobile' || layout === 'single-column'}>
                    {children}
                </SwitchingColumnsArea>
            </div>
        );
    }
}
