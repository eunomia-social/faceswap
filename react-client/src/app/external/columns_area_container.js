import { connect } from 'react-redux';
import ColumnsArea from './columns_area';
import {List} from "immutable";

const mapStateToProps = (state) => ({
  columns: state.get('columns', List([])),
  isModalOpen: false
});

export default connect(mapStateToProps, null, null, { forwardRef: true })(ColumnsArea);
