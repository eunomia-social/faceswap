import axios from 'axios';

const plainDomain = (url) => {
    if (url) {
        const regex = /(https|http):\/\//g;
        return url.replace(regex, '').split('/')[0];
    }
    return url;
};

export class RefaceClient {
    _refaceUrl;
    _accessToken;
    _mastodonDomain;
    _requestTimeout = 30000;
    constructor(refaceUrl, accessToken, mastodonUrl) {
        this._accessToken = accessToken;
        this._mastodonDomain = plainDomain(mastodonUrl);
        this._refaceUrl = refaceUrl;
    }
    fileUrl(name) {
        return `${this._refaceUrl}/reface/files/${name}?domain=${this._mastodonDomain}&token=${this._accessToken}`;
    }
    get headers() {
        return { 'X-SN-TOKEN': this._accessToken, 'X-SN-URL': `https://${this._mastodonDomain}` };
    }

    async getVideos() {
        const url = `${this._refaceUrl}/reface/videos`;
        try {
            const response = await axios.get(
                url,
                {headers: this.headers, timeout: this._requestTimeout}
            );
            return response.data;
        } catch (e) {
            return [];
        }
    }

    async uploadImage(file) {
        let formData = new FormData();
        formData.append('file', file);
        const url =  `${this._refaceUrl}/reface/upload`;
        const headers = {
            'Content-Type': 'multipart/form-data',
            'X-SN-TOKEN': this._accessToken,
            'X-SN-URL': `https://${this._mastodonDomain}`,
        };
        try {
            const response = await axios.post(url, formData, {headers});
            return response.data;
        } catch (e) {
            return undefined;
        }
    }

    async swapFaces(imageFile, videoFile) {
        const url = `${this._refaceUrl}/reface/swap?image=${imageFile}&video=${videoFile}`;
        try {
            const response = await axios.get(url, {headers: this.headers, timeout: this._requestTimeout});
            return response.data;
        } catch (e) {
            return undefined;
        }
    }


    async getStatus(jobId) {
        const url = `${this._refaceUrl}/reface/status/${jobId}`;
        try {
            const response = await axios.get(url, {headers: this.headers, timeout: this._requestTimeout});
            return response.data;
        } catch (e) {
            return undefined;
        }
    }

    async deleteResult(filename) {
        const url = `${this._refaceUrl}/reface/files/${filename}`;
        try {
            const response = await axios.delete(url, {headers: this.headers, timeout: this._requestTimeout});
            return response.data;
        } catch (e) {
            return undefined;
        }
    }

    async shareResult(filename) {
        const url = `${this._refaceUrl}/reface/share/${filename}`;
        try {
            const response = await axios.post(url, null, {headers: this.headers, timeout: this._requestTimeout});
            return response.data;
        } catch (e) {
            return undefined;
        }   
    }
}
