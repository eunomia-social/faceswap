import React from 'react';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import {  withRouter } from 'react-router-dom';
import ImmutablePureComponent from 'react-immutable-pure-component';
import Video from '../external/video';
import Column from "../external/column";
import ColumnHeader from "../external/column_header";
import IconButton from "../external/icon_button";
import {eunomiaIcon} from "./eunomia_icon";
import {SessionStore} from "../index";
import {RefaceClient} from "../reface_client_lib";

@injectIntl
class RefaceResults extends ImmutablePureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    intl: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
    shouldUpdateScroll: PropTypes.func.isRequired,
  };

  state = {
    media: [],
    fetched: false,
  };

  _mounted = false;
  _interval = null;
  updateOnState = ['fetched', 'media'];

  constructor(props) {
    super(props);
    const { refaceUrl, refaceMastodonUrl, refaceMastodonToken } = SessionStore;
    this.client = new RefaceClient(refaceUrl, refaceMastodonToken, refaceMastodonUrl);
    this._checkStatus = this._checkStatus.bind(this);
    this.deleteResult = this.deleteResult.bind(this);
    this.shareResult = this.shareResult.bind(this);
  }

  _checkStatus() {
    if (this._mounted && !this.state.fetched) {
      const { task } = this.props.location.state;
      this.client.getStatus(task)
      .then((data) => {
        if (this._mounted) {
          const { status } = data;
          if (!status || status === 'not_found') {
            this.context.router.history.goBack();
          } else {
            if (status === 'complete') {
              if (!data.result) {
                this.context.router.history.goBack();
              } else {
                this.setState({
                  media: [data.result],
                  fetched: true,
                });
              }
            }
          }
        }
      }).catch((reason) => {
        console.warn(reason);
        this.context.router.history.goBack();
      });
    }
  }

  componentDidMount() {
    const { task } = this.props.location.state;
    if (!task) {
      this.context.router.history.goBack();
    } else {
      this._interval = setInterval(this._checkStatus, 3000);
      this._mounted = true;
    }
  }

  componentWillUnmount () {
    if (this._interval) {
      clearInterval(this._interval);
    }
    this._mounted = false;
  }

  setRef = (c) => {
    this.node = c;
  }

  shareResult() {
    if (this.state.media && this.state.media.length === 1) {
      const filename = this.state.media[0].name;
      this.client.shareResult(filename)
          .then((data) => {
            console.log(data);
          })
          .catch((reason)=> {
            console.warn(reason);
            this.context.router.history.goBack();
          });
    } else {
      this.context.router.history.goBack();
    }
  }

  deleteResult() {
    if (this.state.media && this.state.media.length === 1) {
      const filename = this.state.media[0].name;
      this.client.deleteResult(filename)
      .then(() => {
        this.context.router.history.goBack();
      }).catch((reason)=> {
        console.warn(reason);
        this.context.router.history.goBack();
      });
    } else {
      this.context.router.history.goBack();
    }
  }

  render() {
    const { intl, location } = this.props;
    if (!location || !location.state || !location.state.attachment) {
      this.context.router.history.push('/reface');
      return null;
    }
    const { attachment } = location.state;
    return (
        <div style={{ padding: 10 }}>
          {this.state.media.map((result, index) => {
            return (
              <div key={`${result.id}-${index}`} style={{ padding: 10 }} className='reface-mobile'>
                <Video
                    preview={this.client.fileUrl(result.preview)}
                    frameRate={result.frameRate}
                    blurhash={result.blurhash}
                    src={this.client.fileUrl(result.name)}
                    alt={result.title}
                    width={attachment.width}
                    height={attachment.height}
                    inline
                    onOpenVideo={null}
                    sensitive={false}
                    alwaysVisible
                    visible
                    //eslint-disable-next-line react/jsx-no-bind
                    onToggleVisibility={()=>{}}
                    intl={intl}
                />
                <div className='reface_controls status__action-bar' >
                  <IconButton
                      className='status__action-bar-button'
                      size={24}
                      title={'Delete'}
                      icon={'remove'}
                      onClick={this.deleteResult}
                  />
                  <IconButton
                      className='status__action-bar-button'
                      size={24}
                      title={'Share'}
                      icon={'share-alt'}
                      onClick={this.shareResult}
                  />
                </div>
              </div>
            );
          })}
        </div>
    );
  };

}
RefaceResults.defaultProps = {
  location: { state: null },
};
export default withRouter(RefaceResults);
