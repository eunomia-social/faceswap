import * as faceApi from 'face-api.js';
import React from 'react';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import {Link, withRouter} from 'react-router-dom';
import IconButton from '../external/icon_button';
import { REFACE_TARGET_WIDTH, REFACE_TARGET_HEIGHT } from '../constants';
import {RefaceClient} from "../reface_client_lib";
import {SessionStore} from "../index";

@injectIntl
class RefaceUser extends React.PureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    intl: PropTypes.object.isRequired,
    shouldUpdateScroll: PropTypes.func.isRequired,
    location: PropTypes.object.isRequired,
  };

  constructor(props) {
    super(props);
    const { refaceUrl, refaceMastodonUrl, refaceMastodonToken } = SessionStore;
    this.client = new RefaceClient(refaceUrl, refaceMastodonToken, refaceMastodonUrl);
    this.run = this.run.bind(this);
    this.onPlay = this.onPlay.bind(this);
    this.runLoop = this.runLoop.bind(this);
    this.usePhoto = this.usePhoto.bind(this);
    this.clearPhoto = this.clearPhoto.bind(this);
    this.takePicture = this.takePicture.bind(this);
    this.uploadPicture = this.uploadPicture.bind(this);
    this.stopStreamedVideo = this.stopStreamedVideo.bind(this);
  }

  video = React.createRef();
  parent = React.createRef();
  canvas = null;
  photo = null;
  photoParent = null;
  _interval = null;
  options = new faceApi.TinyFaceDetectorOptions({
    inputSize: 512,
    scoreThreshold: 0.6,
    minFaceSize: 100,
  });
  displaySize = { width: 0, height: 0 };
  state = { streaming: false, videoDevices: [], face: null, facingMode: 'user' };

  componentDidMount() {
    const { attachment } = this.props.location;
    if (!attachment) {
      this.context.router.history.goBack();
    } else {
      this.run();
    }
  }

  stopStreamedVideo() {
    if (this.video.current) {
      const stream = this.video.current.srcObject;
      if (stream) {
        const tracks = stream.getTracks();
        tracks.forEach((track) => {
          track.stop();
        });
      }
      this.video.current.srcObject = null;
    }
  }

  componentWillUnmount() {
    if (this._interval) {
      clearInterval(this._interval);
    }
    this.stopStreamedVideo();
  }

  run() {
    // try {
    faceApi.nets.tinyFaceDetector.load('/models/').then(() => {
      navigator.mediaDevices.getUserMedia({ audio: false, video: { facingMode: this.state.facingMode } }).then((mediaStream) => {
        this.mediaStream = mediaStream;
        this.video.current.srcObject = this.mediaStream;
        navigator.mediaDevices.enumerateDevices().then ((devices) => {
          const videoDevices = devices.filter(device => device.kind === 'videoinput');
          this.setState({ streaming: videoDevices.length > 0, videoDevices });
        }).catch((reason) => {
          console.warn(reason);
        });
      }).catch((reason) => {
        console.warn(reason);
      });
    }).catch((reason) => {
      console.warn(reason);
    });
  }

  runLoop() {
    if (this.video.current) {
      if (this.state.streaming) {
        faceApi.detectSingleFace(this.video.current, this.options).then(detection => {
          if (detection) {
            const face = faceApi.resizeResults(detection, this.displaySize);
            this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);
            const drawBox = new faceApi.draw.DrawBox(face.box, { label: '' });
            drawBox.draw(this.canvas);
            this.setState({ face });
          } else {
            this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);
            this.setState({ face: null });
          }
        }).catch((reason) => {
          console.warn(reason);
          this.setState({ face: null });
        });
      }
    }
  }

  onPlay () {
    this.canvas = faceApi.createCanvasFromMedia(this.video.current);
    const videoRect = this.video.current.getBoundingClientRect();
    this.displaySize = { width: videoRect.width, height: videoRect.height };
    this.canvas.style.marginTop = `-${this.displaySize.height}px`;
    this.parent.current.appendChild(this.canvas);
    faceApi.matchDimensions(this.canvas, this.displaySize);
    this._interval = setInterval(this.runLoop, 200);
  }

  uploadPicture(cb) {
    if (this.photo) {
      fetch(this.photo.src)
        .then(res => res.blob())
        .then((blob) => {
          const file = new File([blob], 'user.png', { type: 'image/png' });
          this.client.uploadImage(file).then((data) => {
            cb(data);
          }). catch((reason) => {
            console.warn(reason);
            cb(null);
          })
        }).catch((reason) => {
          console.warn(reason);
          cb(null);
        });
    }
  }

  usePhoto() {
    const { attachment } = this.props.location;
    if (!attachment || !attachment.name) {
      this.context.router.history.goBack();
    }
    this.uploadPicture((imageId) => {
      if (!imageId) {
        this.context.router.history.goBack();
      }
      this.client.swapFaces(imageId, attachment.name).then((data) => {
        if (!data || !data.job_id) {
          this.context.router.history.goBack();
        } else {
          this.context.router.history.push('/reface/results', { task: data.job_id, attachment });
        }
      }).catch((reason) => {
        console.warn(reason);
        this.context.router.history.goBack();
      });
    });
  }

  clearPhoto() {
    if (this.photoParent) {
      if (this.photo) {
        this.photoParent.removeChild(this.photo);
        this.photo = null;
      }
      this.parent.current.removeChild(this.photoParent);
    }
    this.setState({ streaming: true });
  }

  takePicture() {
    if (this.state.streaming) {
      this.setState({ streaming: false });
      if (this.state.face !== null) {
        // clear current box
        this.canvas.getContext('2d').clearRect(0, 0, this.canvas.width, this.canvas.height);
        // parent element (size of canvas)
        this.photoParent = document.createElement('div');
        this.photoParent.style.marginTop = `-${this.displaySize.height}px`;
        this.photoParent.style.width = `${this.displaySize.width}px`;
        this.photoParent.style.height = `${this.displaySize.height}px`;
        this.photoParent.style.backgroundColor= 'black';
        // new canvas with detected face on the center
        const faceX = this.state.face._box._x;
        const faceY = this.state.face._box._y;
        const faceWidth = this.state.face._box._width;
        const faceHeight = this.state.face._box._height;
        const sourceX = faceX - (REFACE_TARGET_WIDTH - faceWidth) / 2;
        const sourceY = faceY - (REFACE_TARGET_HEIGHT - faceHeight) / 2;
        const trimmedCanvas = document.createElement('canvas');
        trimmedCanvas.width = REFACE_TARGET_WIDTH;
        trimmedCanvas.height = REFACE_TARGET_HEIGHT;
        trimmedCanvas.getContext('2d').drawImage(
          this.video.current,
          sourceX,
          sourceY,
          REFACE_TARGET_WIDTH,
          REFACE_TARGET_HEIGHT,
          0,
          0,
          REFACE_TARGET_WIDTH,
          REFACE_TARGET_HEIGHT,
        );
        // photo element (256x256)
        this.photo = document.createElement('img');
        this.photo.src = trimmedCanvas.toDataURL('image/png');
        this.photo.style.width = REFACE_TARGET_WIDTH;
        this.photo.style.height = REFACE_TARGET_HEIGHT;
        this.photo.style.marginTop = `${(this.displaySize.height - REFACE_TARGET_HEIGHT) / 2}px`;
        this.photo.style.marginLeft = `${(this.displaySize.width - REFACE_TARGET_WIDTH) / 2}px`;
        // add elements
        this.photoParent.appendChild(this.photo);
        this.parent.current.appendChild(this.photoParent);
      } else {
        this.clearPhoto();
      }
    } else {
      this.clearPhoto();
    }
  }

  render() {
    return (
      <div>
          <Link className='btn-link' to={{ pathname: '/reface' }}>Back</Link>
          <div className={'reface-mobile'}>
            <div ref={this.parent} className={'reface'}>
              <video
                ref={this.video}
                autoPlay
                muted
                playsInline
                onPlay={this.onPlay}
              />
            </div>
            <div className='reface_controls status__action-bar' >
              { this.canvas && this.state.videoDevices.length > 0 &&
                <IconButton
                  className='status__action-bar-button'
                  size={22}
                  title={this.state.streaming ? 'Take photo': 'Clear'}
                  icon={this.state.streaming ? 'camera-retro': 'eraser'}
                  onClick={this.takePicture}
                  disabled={!this.state.face}
                  pressed={false}
                />
              }
              {
                this.canvas && this.state.videoDevices.length > 0 &&
                <IconButton
                  className='status__action-bar-button'
                  size={24}
                  title={'Swap!'}
                  icon={'magic'}
                  onClick={this.usePhoto}
                  disabled={!this.state.face || ! this.photo}
                  style={{ marginRight: 0 }}
                />
              }
            </div>
          </div>
      </div>
    );
  };

}

export default withRouter(RefaceUser);
