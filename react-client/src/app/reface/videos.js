import React from 'react';
import { injectIntl } from 'react-intl';
import PropTypes from 'prop-types';
import ImmutablePureComponent from 'react-immutable-pure-component';
import Video from '../external/video';
import { Link } from 'react-router-dom';

import {SessionStore} from "..";
import {RefaceClient} from "../reface_client_lib";

export default @injectIntl
class RefaceVideos extends ImmutablePureComponent {

  static contextTypes = {
    router: PropTypes.object,
  };

  static propTypes = {
    intl: PropTypes.object.isRequired,
    shouldUpdateScroll: PropTypes.func.isRequired,
  };
  state = {
    media: [],
    fetched: false,
  };

  updateOnState = ['fetched', 'media'];

  constructor(props) {
    super(props);
    const { refaceUrl, refaceMastodonUrl, refaceMastodonToken } = SessionStore;
    this.client = new RefaceClient(refaceUrl, refaceMastodonToken, refaceMastodonUrl);
  }

  componentDidMount() {
    this.client.getVideos()
        .then((results) => {
          this.setState({ fetched: true, media: results });
        })
        .catch(()=>{
          this.setState({ fetched: true, media: [] });
        });
  }

  setRef = (c) => {
    this.node = c;
  }

  render() {
    const { intl } = this.props;
    return (
      <div style={{ padding: 10 }}>
          {this.state.media.map((attachment, index) => {
            return (
              <div key={`${attachment.id}-${index}`} style={{ padding: 10 }} className='reface-mobile'>
                <Video
                  preview={this.client.fileUrl(attachment.preview)}
                  frameRate={attachment.frameRate}
                  blurhash={attachment.blurhash}
                  src={this.client.fileUrl(attachment.name)}
                  alt={attachment.title}
                  width={attachment.width}
                  height={attachment.height}
                  inline
                  onOpenVideo={null}
                  sensitive={false}
                  alwaysVisible
                  visible
                  //eslint-disable-next-line react/jsx-no-bind
                  onToggleVisibility={()=>{}}
                  intl={intl}
                />
                <Link className='btn-link column-header__button active' to={{ pathname: '/reface/photo', attachment }}>
                  <div style={{ marginTop: 10, flexWrap: 'wrap' }}>{attachment.title}</div>
                </Link>
              </div>
            );
          })}
      </div>
    );
  };

}
