import React from 'react';
import ReactDOM from 'react-dom';

require('font-awesome/css/font-awesome.css');
require.context('./images/', true);

import App from './app';

function main() {
  document.addEventListener('DOMContentLoaded', () => {
    const mountNode = document.getElementById('root');
    ReactDOM.render(<App />, mountNode);
  });
}

main();
