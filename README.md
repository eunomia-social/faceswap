# EUNOMIA Reface

!["diagram](./diagram/diagram.jpg "diagram")

4 components [./docker-compose.yml](./docker-compose.yml):

- Reface
- Worker
- Redis
- Reface_cpu

The first three are to be used inside a GPU host <br />
The last can either also run on the same machine, or integrated with the rest of the EUNOMIA components:
It handles the client requests (i.e., get videos, upload photo, trigger the face swap).<br />
It is also responsible for the forwarding of the results back to the clients.<br />
If not, (not integrated [ESN_GATEWAY_ADDRESS, ESN_GATEWAY_PORT](./env.template)), the clients(js) should periodically check the job's status, till we have the results <br />

## Mutual TLS

use [bash ./certs/make_certs.sh](./certs/make_certs.sh) to generate client and server certificates:

4 files will be generated: `server.crt, server.key, client.crt, client.key`

- the client uses: server.crt, client.crt, client.key
- the server uses: server.crt, server.key, client.crt

If multiple clients (and not only the one included in [./docker-compose.yml](docker-compose.yml)) should be connecting the grpc server, the ([COMMON_NAME](./env.template)) should then be a FQDM (`something.example.com`) and not `reface`

## React Client

A simple react [./react-client](./react-client) is included with face detection and a js [client](./react-client/src/app/reface_client_lib.js) to communicate with the http backend.
