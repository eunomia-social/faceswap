#!/bin/bash
_HERE="$(dirname "$(readlink -f "$0")")"
if [ "${_HERE}" = "." ]; then
  _HERE="$(pwd)"
fi
cd "${_HERE}" || exit 1

COMMON_NAME="${COMMON_NAME:-reface}"
CERT_PREFIX="${CERT_PREFIX:-}"
if ! command -v openssl > /dev/null 2>&1; then
  echo "openssl command not found"
  exit 1
fi
openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout "${CERT_PREFIX}server.key" -subj "/CN=${COMMON_NAME}" -out "${CERT_PREFIX}server.crt"
openssl req -x509 -newkey rsa:4096 -sha256 -nodes -keyout "${CERT_PREFIX}client.key" -subj "/CN=${COMMON_NAME}" -out "${CERT_PREFIX}client.crt"
